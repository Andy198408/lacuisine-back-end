package be.greenteam.lacuisine.model;
/**
 * Created by "Andy Van Camp" on 20/08/2021.
 */

import lombok.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Reservation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Restaurant restaurant;

    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    private User user;

    private LocalDate date;

    private LocalTime startTime;

    private LocalTime endTime;

    private int partySize;

    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    private RestaurantTable restaurantTable;

    @Override
    public String toString() {
        return "Reservation{" +
                "id=" + id +
                ", user=" + user +
                ", date=" + date +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", partySize=" + partySize +
                '}';
    }
}
