package be.greenteam.lacuisine.model;
/**
 * Author: Andy.
 */

public enum ERole {
    ROLE_USER,
    ROLE_MODERATOR,
    ROLE_ADMIN
}
