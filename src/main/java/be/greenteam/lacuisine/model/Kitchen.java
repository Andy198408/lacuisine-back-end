package be.greenteam.lacuisine.model;

public enum Kitchen {
    BELGIAN("Belgian"),
    ITALIAN("Italian"),
    GREEK("Greek"),
    INDIAN("Indian"),
    THAIS("Thais"),
    CHINESE("Chinese"),
    AMERICAN("American"),
    SPANISH("Spanish"),
    FRENCH("French"),
    MEXICAN("Mexican"),
    BRITISH("British");

    private String value;

    Kitchen(String value){
        this.value = value;
    }

    public String getValue(){
        return value;
    }
}

