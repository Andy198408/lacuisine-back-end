package be.greenteam.lacuisine.model;

public enum FoodType {
    DRINK,
    FOOD
}
