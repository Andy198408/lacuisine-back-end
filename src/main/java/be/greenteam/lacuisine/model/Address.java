package be.greenteam.lacuisine.model;

import lombok.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

/**
 * Created by "Andy Van Camp" on 17/08/2021.
 */

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Address {

    @Id
    @Column(name = "restaurant_id")
    private Long id;

    private String street;
    private int nr;
    private int zipcode;
    private String city;
    private String country;

    @OneToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    @MapsId
    @JoinColumn(name = "restaurant_id")
    private Restaurant restaurant;

}
