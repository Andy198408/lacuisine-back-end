package be.greenteam.lacuisine.model;

import lombok.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Created by "Andy Van Camp" on 13/08/2021.
 */

@Entity
@Getter
@Setter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Advertisement {

    @Id
    @Column(name = "restaurant_Id")
    private Long id;

    private String description;

    private LocalDateTime expireDate;

    private final LocalDateTime creation = LocalDateTime.now();

    @OneToOne
    @MapsId
    @JoinColumn(name = "restaurant_Id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Restaurant restaurant;


}
