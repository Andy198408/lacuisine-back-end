package be.greenteam.lacuisine;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class LacuisineApplication {

    public static void main(String[] args) {
        SpringApplication.run(LacuisineApplication.class, args);
    }

}
