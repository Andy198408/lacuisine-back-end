/**
 * authors Shiwi/olivier
 */

package be.greenteam.lacuisine.bootstrap;

import be.greenteam.lacuisine.model.*;
import be.greenteam.lacuisine.payload.request.SignupRequest;
import be.greenteam.lacuisine.repository.RoleRepository;
import be.greenteam.lacuisine.repository.UserRepository;
import be.greenteam.lacuisine.service.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static be.greenteam.lacuisine.model.FoodType.DRINK;
import static be.greenteam.lacuisine.model.FoodType.FOOD;
import static be.greenteam.lacuisine.model.Kitchen.ITALIAN;


@Component
@Profile({"dev", "prod"})
@RequiredArgsConstructor
@Slf4j
@Order(1)
public class Bootstrap implements CommandLineRunner {
    private final MenuItemService menuItemService;
    private final MenuService menuService;
    private final RatingService ratingService;
    private final RestaurantService restaurantService;
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final WalletService walletService;
    private final PasswordEncoder encoder;
    private final AddressService addressService;
    private final AdvertisementService advertisementService;
    private final OpeninghoursService openinghoursService;
    private final RestaurantTableService restaurantTableService;
    private final ReservationService reservationService;


    @Override
    @Transactional
    public void run(String... args) throws Exception {
        init();
    }

    private void init() {

        HashSet<String> roles = new HashSet<>();
        roles.add("admin");
        roles.add("mod");

        for (int i = 1; i < 5; i++) {
            SignupRequest test = SignupRequest.builder().username("test" + i).email("test@test.com" + i).password("123456").role(roles).build();
            User user = registerUser(test);
            Restaurant testResto = Restaurant.builder().name("TestResto" + i).kitchen(ITALIAN)
                    .owner(user).description("Restaurant for testing").build();
            Restaurant savedRestaurant = restaurantService.save(testResto);

            Menu menu = Menu.builder().name("TestResto" + i).restaurant(savedRestaurant).build();
            Menu savedMenu = menuService.save(menu);

            saveMenuItems(savedMenu);
            saveTable(savedRestaurant);
            saveOpeningHours(savedRestaurant);
            saveAddress(savedRestaurant, i);
            saveRating(savedRestaurant, user);
            saveAdvertisement(savedRestaurant);
            saveReservations();

        }

        log.debug("data loaded");

    }

    private void saveReservations() {
        final Reservation reservation = Reservation.builder()
                .date(LocalDate.of(2021, 11, 14))
                .restaurant(restaurantService.findById(1L))
                .user(userRepository.findById(1L).get())
                .startTime(LocalTime.now())
                .endTime(LocalTime.now().plusHours(1))
                .partySize(5)
                .restaurantTable(restaurantTableService.findById(1L))
                .build();
        reservationService.save(reservation);
    }

    private void saveAdvertisement(Restaurant savedRestaurant) {
        Advertisement advertisement = Advertisement.builder()
                .restaurant(savedRestaurant)
                .expireDate(LocalDateTime.now().plusMinutes(1))
                .description("Test advertisement for:" + savedRestaurant.getName())
                .build();
        advertisementService.save(advertisement);
    }

    private void saveRating(Restaurant savedRestaurant, User user) {
        Rating rating = Rating.builder()
                .restaurant(savedRestaurant)
                .score(3).feedback("dit is een test!")
                .user(user).date(LocalDate.now())
                .build();
        ratingService.save(rating);
        log.debug("rating loaded");
    }

    private void saveAddress(Restaurant savedRestaurant, int i) {
        Address addressFirst = Address.builder()
                .city("test" + i).country("test" + i)
                .nr(i).street("test" + i).zipcode(i)
                .restaurant(savedRestaurant)
                .build();
        addressService.save(addressFirst);

        log.debug("address loaded");
    }

    private void saveMenuItems(Menu savedMenu) {
        MenuItem cola = MenuItem.builder().name("cola").foodType(DRINK).vegetarian(true).price(1.80).menu(savedMenu).build();
        MenuItem fanta = MenuItem.builder().name("fanta").foodType(DRINK).vegetarian(true).price(1.80).menu(savedMenu).build();
        MenuItem water = MenuItem.builder().name("water").foodType(DRINK).vegetarian(true).price(1.50).menu(savedMenu).build();
        MenuItem bier = MenuItem.builder().name("bier").foodType(DRINK).vegetarian(true).price(2.10).menu(savedMenu).build();
        MenuItem pizza = MenuItem.builder().name("pizza").foodType(FOOD).vegetarian(true).price(10.00).menu(savedMenu).build();
        MenuItem pasta = MenuItem.builder().name("pasta").foodType(FOOD).vegetarian(true).price(8.60).menu(savedMenu).build();
        MenuItem frieten = MenuItem.builder().name("frieten").foodType(FOOD).vegetarian(false).price(2.00).menu(savedMenu).build();
        MenuItem salade = MenuItem.builder().name("salade").foodType(FOOD).vegetarian(true).price(4.80).menu(savedMenu).build();
        MenuItem volAuVent = MenuItem.builder().name("vol au vent").foodType(FOOD).vegetarian(false).price(7.80).menu(savedMenu).build();
        menuItemService.saveAll(List.of(cola, fanta, water, bier, pizza, pasta, frieten, salade, volAuVent));

        log.debug("menu items loaded");
    }

    private void saveOpeningHours(Restaurant savedRestaurant) {
        Openinghours openinghours = Openinghours.builder().restaurant(savedRestaurant).openTime(LocalTime.of(8, 15)).closeTime(LocalTime.of(16, 0)).weekday(DayOfWeek.MONDAY).reservationTimeslot(60).active(true).build();
        Openinghours openinghours2 = Openinghours.builder().restaurant(savedRestaurant).openTime(LocalTime.of(8, 15)).closeTime(LocalTime.of(16, 0)).weekday(DayOfWeek.TUESDAY).reservationTimeslot(60).active(true).build();
        Openinghours openinghours3 = Openinghours.builder().restaurant(savedRestaurant).openTime(LocalTime.of(8, 15)).closeTime(LocalTime.of(16, 0)).weekday(DayOfWeek.WEDNESDAY).reservationTimeslot(60).active(true).build();
        Openinghours openinghours4 = Openinghours.builder().restaurant(savedRestaurant).openTime(LocalTime.of(8, 15)).closeTime(LocalTime.of(16, 0)).weekday(DayOfWeek.THURSDAY).reservationTimeslot(60).active(true).build();
        Openinghours openinghours5 = Openinghours.builder().restaurant(savedRestaurant).openTime(LocalTime.of(8, 15)).closeTime(LocalTime.of(16, 0)).weekday(DayOfWeek.FRIDAY).reservationTimeslot(60).active(true).build();
        Openinghours openinghours6 = Openinghours.builder().restaurant(savedRestaurant).openTime(LocalTime.of(8, 15)).closeTime(LocalTime.of(16, 0)).weekday(DayOfWeek.SATURDAY).reservationTimeslot(60).active(true).build();
        Openinghours openinghours7 = Openinghours.builder().restaurant(savedRestaurant).openTime(LocalTime.of(8, 15)).closeTime(LocalTime.of(16, 0)).weekday(DayOfWeek.SUNDAY).reservationTimeslot(60).active(true).build();
        openinghoursService.saveAll(List.of(openinghours, openinghours2, openinghours3, openinghours4, openinghours5, openinghours6, openinghours7));

        log.debug("opening hours loaded");
    }

    private void saveTable(Restaurant savedRestaurant) {
        RestaurantTable restaurantTable = RestaurantTable.builder().restaurant(savedRestaurant).capacity(2).build();
        RestaurantTable restaurantTable2 = RestaurantTable.builder().restaurant(savedRestaurant).capacity(4).build();
        RestaurantTable restaurantTable3 = RestaurantTable.builder().restaurant(savedRestaurant).capacity(6).build();
        restaurantTableService.saveAll(List.of(restaurantTable, restaurantTable2, restaurantTable3));

        log.debug("tables loaded");
    }


    private User registerUser(SignupRequest signUpRequest) {

        if (userRepository.existsByUsername(signUpRequest.getUsername())) {
            throw new RuntimeException("User exists");
        }

        if (userRepository.existsByEmail(signUpRequest.getEmail())) {
            throw new RuntimeException("email exists");
        }

        // Create new user's account
        User user = User.builder()
                .username(signUpRequest.getUsername())
                .email(signUpRequest.getEmail())
                .password(encoder.encode(signUpRequest.getPassword()))
                .active(true)
                .build();

        Set<String> strRoles = signUpRequest.getRole();
        Set<Role> roles = new HashSet<>();

        if (strRoles == null) {
            Role userRole = roleRepository.findByName(ERole.ROLE_USER)
                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(userRole);
        } else {
            strRoles.forEach(role -> {
                switch (role) {
                    case "admin":
                        Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(adminRole);

                        break;
                    case "mod":
                        Role modRole = roleRepository.findByName(ERole.ROLE_MODERATOR)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(modRole);

                        break;
                }
            });
            Role userRole = roleRepository.findByName(ERole.ROLE_USER)
                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(userRole);
        }

        user.setRoles(roles);
        User savedUser = userRepository.save(user);
        Wallet wallet = new Wallet();
        wallet.setUser(savedUser);
        wallet.setStoreCredit(100);
        walletService.save(wallet);
        log.debug("user loaded");
        return savedUser;
    }
}




