package be.greenteam.lacuisine.service;

import be.greenteam.lacuisine.model.Restaurant;

import java.util.List;

public interface RestaurantService extends CrudService<Restaurant, Long> {
    List<Restaurant> findAllByOwnerId(Long id);
}
