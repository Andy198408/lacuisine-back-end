package be.greenteam.lacuisine.service;

import be.greenteam.lacuisine.model.Menu;
import be.greenteam.lacuisine.repository.MenuRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.Collection;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MenuServiceImpl implements MenuService {

    private final MenuRepository menuRepo;

    @Override
    public Menu save(Menu entity) {
        return menuRepo.save(entity);
    }

    @Override
    public List<Menu> saveAll(Collection<Menu> entities) {
        return menuRepo.saveAll(entities);
    }

    @Override
    public Menu findById(Long aLong) {
        return menuRepo.findById(aLong).orElseThrow(
                () -> new EntityNotFoundException("No menu found for id:" + aLong)
        );
    }

    @Override
    public List<Menu> findAll() {
        return menuRepo.findAll();
    }

    @Override
    public boolean existsById(Long aLong) {
        return menuRepo.existsById(aLong);
    }

    @Override
    public void deleteById(Long aLong) {
        menuRepo.deleteById(aLong);
    }

    @Override
    public void delete(Menu entity) {
        menuRepo.delete(entity);
    }

    @Override
    public void deleteAll(Iterable<Menu> entities) {
        menuRepo.deleteAll(entities);
    }
}
