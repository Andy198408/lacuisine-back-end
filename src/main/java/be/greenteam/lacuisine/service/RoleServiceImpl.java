package be.greenteam.lacuisine.service;

import be.greenteam.lacuisine.model.ERole;
import be.greenteam.lacuisine.model.Role;
import be.greenteam.lacuisine.repository.RoleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.Collection;
import java.util.List;

/**
 * Created by andyv on 23/07/2021.
 */
@Service
@RequiredArgsConstructor
public class RoleServiceImpl implements RoleService {

    private final RoleRepository roleRepository;

    @Override
    public Role save(Role entity) {
        return roleRepository.save(entity);
    }

    @Override
    public List<Role> saveAll(Collection<Role> entities) {
        return roleRepository.saveAll(entities);
    }

    @Override
    public Role findById(Long aLong) {
        return roleRepository.getById(aLong);
    }

    @Override
    public List<Role> findAll() {
        return roleRepository.findAll();
    }

    @Override
    public boolean existsById(Long aLong) {
        return roleRepository.existsById(aLong);
    }

    @Override
    public void deleteById(Long aLong) {
        roleRepository.deleteById(aLong);
    }

    @Override
    public void delete(Role entity) {
        roleRepository.delete(entity);
    }

    @Override
    public void deleteAll(Iterable<Role> entities) {
        roleRepository.deleteAll(entities);
    }

    @Override
    public Role findByName(ERole name) {
        return roleRepository.findByName(name).orElseThrow(() -> new EntityNotFoundException("No role found with name: " + name));
    }
}
