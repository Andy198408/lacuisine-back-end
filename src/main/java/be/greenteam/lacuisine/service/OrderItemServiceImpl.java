package be.greenteam.lacuisine.service;

import be.greenteam.lacuisine.model.OrderItem;
import be.greenteam.lacuisine.repository.OrderItemRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.Collection;
import java.util.List;

@Service
@RequiredArgsConstructor
public class OrderItemServiceImpl implements OrderItemService{

    private final OrderItemRepository orderItemRepo;

    @Override
    public List<OrderItem> findOrderItemByOrderId(Long id) {
        return orderItemRepo.findOrderItemByOrderId(id);
    }

    @Override
    public OrderItem save(OrderItem entity) {
        return orderItemRepo.save(entity);
    }

    @Override
    public List<OrderItem> saveAll(Collection<OrderItem> entities) {
        return orderItemRepo.saveAll(entities);
    }

    @Override
    public OrderItem findById(Long aLong) {
        return orderItemRepo.findById(aLong).orElseThrow(
                () -> new EntityNotFoundException("No OrderItem found for id:" + aLong)
        );
    }

    @Override
    public List<OrderItem> findAll() {
        return orderItemRepo.findAll();
    }

    @Override
    public boolean existsById(Long aLong) {
        return orderItemRepo.existsById(aLong);
    }

    @Override
    public void deleteById(Long aLong) {
        orderItemRepo.deleteById(aLong);
    }

    @Override
    public void delete(OrderItem entity) {
        orderItemRepo.delete(entity);
    }

    @Override
    public void deleteAll(Iterable<OrderItem> entities) {
        orderItemRepo.deleteAll(entities);
    }
}
