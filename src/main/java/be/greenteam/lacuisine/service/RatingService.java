package be.greenteam.lacuisine.service;

import be.greenteam.lacuisine.model.Rating;

import java.util.List;

public interface RatingService extends CrudService<Rating, Long> {
    List<Rating> findAllByRestaurantId(Long restaurantId);
    List<Rating> findAllByUserId(Long userId);
}
