package be.greenteam.lacuisine.service;

import be.greenteam.lacuisine.model.RestaurantTable;

import java.util.List;

/**
 * Created by "Andy Van Camp" on 18/08/2021.
 */
public interface RestaurantTableService extends CrudService<RestaurantTable, Long> {
    List<RestaurantTable> findAllByRestaurantId(Long id);
    List<RestaurantTable> findAllByRestaurantIdAndCapacity(Long id, int capacity);
}
