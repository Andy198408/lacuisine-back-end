package be.greenteam.lacuisine.service;

import be.greenteam.lacuisine.model.MenuItem;
import be.greenteam.lacuisine.model.Order;
import be.greenteam.lacuisine.model.OrderItem;
import be.greenteam.lacuisine.payload.map.MenuItemMapLine;
import be.greenteam.lacuisine.payload.model.MenuItemData;
import be.greenteam.lacuisine.repository.MenuItemRepository;
import be.greenteam.lacuisine.util.dataRetrieval.RetrieveChartData;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class MenuItemServiceImpl implements MenuItemService {

    private final MenuItemRepository menuItemRepo;
    private final OrderService orderService;
    private final OrderItemService orderItemService;
    private final RetrieveChartData retrieveChartData;

    @Override
    public MenuItem save(MenuItem entity) {
        return menuItemRepo.save(entity);
    }

    @Override
    public List<MenuItem> saveAll(Collection<MenuItem> entities) {
        return menuItemRepo.saveAll(entities);
    }

    @Override
    public MenuItem findById(Long aLong) {
        return menuItemRepo.findById(aLong).orElseThrow(
                () -> new EntityNotFoundException("No MenuItem found for id: " + aLong)
        );
    }

    @Override
    public List<MenuItem> findAll() {
        return menuItemRepo.findAll();
    }

    @Override
    public boolean existsById(Long aLong) {
        return menuItemRepo.existsById(aLong);
    }

    @Override
    public void deleteById(Long aLong) {
        menuItemRepo.deleteById(aLong);
    }

    @Override
    public void delete(MenuItem entity) {
        menuItemRepo.delete(entity);
    }

    @Override
    public void deleteAll(Iterable<MenuItem> entities) {
        menuItemRepo.deleteAll(entities);
    }

    @Override
    public List<MenuItem> findAllByMenuId(Long menuId) {
        return menuItemRepo.findAllByMenuId(menuId);
    }

    @Override
    public List<MenuItemMapLine> getLineGraphData(Long restaurantId) {
        List<Order> allOrders = orderService.findAllByRestaurantIdOrderByOrderTimeDesc(restaurantId);
        List<OrderItem> orderItems = allOrders.stream()
                .sorted(Comparator.comparing(Order::getOrderTime))
                .map(order -> orderItemService.findOrderItemByOrderId(order.getId()))
                .flatMap(list -> list.stream())
                .collect(Collectors.toList());

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

        HashMap<String, List<MenuItemData>> menuItemData = new HashMap<>();
        orderItems.forEach(orderItem -> {
            String name = orderItem.getMenuItem().getName();
            if (menuItemData.containsKey(name)) {
                log.debug("orderItem: " + orderItem.getOrder().getOrderTime().toLocalDate());
                List<MenuItemData> itemDataList = menuItemData.get(name);
                AtomicBoolean found = new AtomicBoolean(false);
                itemDataList.forEach(menuItemDataLine -> {
                    if (menuItemDataLine.getName().equals(formatter.format(orderItem.getOrder().getOrderTime()))) {
                        menuItemDataLine.setValue(menuItemDataLine.getValue() + (orderItem.getMenuItem().getPrice() * orderItem.getAmount()));
                        found.set(true);
                    }
                });
                if (!found.get()) {
                    itemDataList.add(MenuItemData.builder()
                            .name(formatter.format(orderItem.getOrder().getOrderTime()))
                            .value(orderItem.getMenuItem().getPrice() * orderItem.getAmount())
                            .build()
                    );
                }
            } else {
                ArrayList<MenuItemData> menuItemDataList = new ArrayList<>();
                menuItemDataList.add(MenuItemData.builder()
                        .name(formatter.format(orderItem.getOrder().getOrderTime()))
                        .value(orderItem.getMenuItem().getPrice() * orderItem.getAmount())
                        .build()
                );
                menuItemData.put(name, menuItemDataList);
            }
        });
        List<MenuItemMapLine> collect = sortMenuItemByFirstEntryDate(formatter, menuItemData);
        return collect;
    }

    /**
     * @param formatter    format of date used
     * @param menuItemData mapping of the graph to be used
     * @return sorted list natural order
     */
    private List<MenuItemMapLine> sortMenuItemByFirstEntryDate(DateTimeFormatter formatter, HashMap<String, List<MenuItemData>> menuItemData) {
        List<MenuItemMapLine> collect = menuItemData.entrySet().stream()
                .map(entry -> MenuItemMapLine.builder().name(entry.getKey()).series(entry.getValue()).build())
                .collect(Collectors.toList());
        collect = collect.stream()
                .sorted(Comparator.comparing(o -> LocalDate.parse(o.getSeries().get(0).getName(), formatter)))
                .collect(Collectors.toList());
        return collect;
    }

    @Override
    public List<MenuItemData> getPieChartGraphData(Long restaurantId, String type) {
        List<Order> allOrders = orderService.findAllByRestaurantIdOrderByOrderTimeDesc(restaurantId);
        List<OrderItem> orderItems = allOrders.stream()
                .sorted(Comparator.comparing(Order::getOrderTime))
                .map(order -> orderItemService.findOrderItemByOrderId(order.getId()))
                .flatMap(list -> list.stream())
                .collect(Collectors.toList());

        HashMap<String, Integer> map = new HashMap<>();

        switch (type) {
            case "total":
                retrieveChartData.getTotalData(orderItems, map);
                break;
            case "food":
                retrieveChartData.getFoodData(orderItems, map);
                break;
            case "drink":
                retrieveChartData.getDrinkData(orderItems, map);
                break;
            default:
                retrieveChartData.getTotalData(orderItems, map);
                break;
        }


        List<MenuItemData> collect = map.entrySet().stream()
                .map(entry -> MenuItemData.builder().name(entry.getKey()).value(Double.valueOf(entry.getValue())).build())
                .collect(Collectors.toList());
        return collect;
    }

}

 /*
        orderItems.forEach(orderItem -> {
                String name = orderItem.getMenuItem().getName();
                if (menuItemData.containsKey(name)) {
//                int total = menuItemData.get(name).getAmount() + orderItem.getAmount();
                log.debug("orderItem: " + orderItem.getOrder().getOrderTime().toLocalDate());
                List<MenuItemData> itemDataList = menuItemData.get(name);
        itemDataList.add(MenuItemData.builder()
        .amount(orderItem.getAmount())
        .foodType(orderItem.getMenuItem().getFoodType())
        .price(orderItem.getMenuItem().getPrice())
        .orderDate(orderItem.getOrder().getOrderTime())
        .vegetarian(orderItem.getMenuItem().isVegetarian())
        .name(orderItem.getMenuItem().getName())
        .build());
        menuItemData.put(name, itemDataList);

        } else {
        ArrayList<MenuItemData> menuItemDataList = new ArrayList<>();
        menuItemDataList.add(MenuItemData.builder()
        .amount(orderItem.getAmount())
        .foodType(orderItem.getMenuItem().getFoodType())
        .price(orderItem.getMenuItem().getPrice())
        .orderDate(orderItem.getOrder().getOrderTime())
        .vegetarian(orderItem.getMenuItem().isVegetarian())
        .name(orderItem.getMenuItem().getName())
        .build());
        menuItemData.put(name, menuItemDataList);

        }
        });

        */