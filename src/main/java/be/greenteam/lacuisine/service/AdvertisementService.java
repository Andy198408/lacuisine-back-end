package be.greenteam.lacuisine.service;

import be.greenteam.lacuisine.model.Advertisement;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by "Andy Van Camp" on 13/08/2021.
 */
public interface AdvertisementService extends CrudService<Advertisement, Long> {
    List<Advertisement> findAllByExpireDateBefore(LocalDateTime localDateTime);

}
