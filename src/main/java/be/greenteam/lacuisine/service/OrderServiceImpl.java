package be.greenteam.lacuisine.service;

import be.greenteam.lacuisine.model.Order;
import be.greenteam.lacuisine.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.Collection;
import java.util.List;

@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService{

    private final OrderRepository orderRepo;

    @Override
    public Order save(Order entity) {
        return orderRepo.save(entity);
    }

    @Override
    public List<Order> saveAll(Collection<Order> entities) {
        return orderRepo.saveAll(entities);
    }

    @Override
    public Order findById(Long aLong) {
        return orderRepo.findById(aLong).orElseThrow(
                () -> new EntityNotFoundException("No Order found for id:" + aLong)
        );
    }

    @Override
    public List<Order> findAll() {
        return orderRepo.findAll();
    }

    @Override
    public boolean existsById(Long aLong) {
        return orderRepo.existsById(aLong);
    }

    @Override
    public void deleteById(Long aLong) {
        orderRepo.deleteById(aLong);
    }

    @Override
    public void delete(Order entity) {
        orderRepo.delete(entity);
    }

    @Override
    public void deleteAll(Iterable<Order> entities) {
        orderRepo.deleteAll(entities);
    }

    @Override
    public List<Order> findAllByRestaurantIdOrderByOrderTimeDesc(Long restaurantId) {
        return orderRepo.findAllByRestaurantIdOrderByOrderTimeDesc(restaurantId);
    }

    @Override
    public List<Order> findAllByUserIdOrderByOrderTimeDesc(Long userId) {
        return orderRepo.findAllByUserIdOrderByOrderTimeDesc(userId);
    }
}
