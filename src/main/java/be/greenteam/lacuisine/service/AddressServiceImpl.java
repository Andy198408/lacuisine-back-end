package be.greenteam.lacuisine.service;

import be.greenteam.lacuisine.model.Address;
import be.greenteam.lacuisine.repository.AddressRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.Collection;
import java.util.List;

/**
 * Created by "Andy Van Camp" on 9/08/2021.
 */
@Service
@RequiredArgsConstructor
public class AddressServiceImpl implements AddressService {

    private final AddressRepository addressRepository;

    @Override
    public Address save(Address entity) {
        return addressRepository.save(entity);
    }

    @Override
    public List<Address> saveAll(Collection<Address> entities) {
        return addressRepository.saveAll(entities);
    }

    @Override
    public Address findById(Long aLong) {
        return addressRepository.findById(aLong).orElseThrow(
                () -> new EntityNotFoundException("No address found with id: " + aLong)
        );
    }

    @Override
    public List<Address> findAll() {
        return addressRepository.findAll();
    }

    @Override
    public boolean existsById(Long aLong) {
        return addressRepository.existsById(aLong);
    }

    @Override
    public void deleteById(Long aLong) {
        addressRepository.deleteById(aLong);
    }

    @Override
    public void delete(Address entity) {
        addressRepository.delete(entity);
    }

    @Override
    public void deleteAll(Iterable<Address> entities) {
        addressRepository.deleteAll(entities);
    }
}
