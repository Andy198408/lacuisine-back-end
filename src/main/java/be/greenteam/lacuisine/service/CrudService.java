package be.greenteam.lacuisine.service;
/**
 * Author: Andy.
 */
import java.util.Collection;
import java.util.List;

public interface CrudService<T, ID> {

     T save(T entity);

    List<T> saveAll(Collection<T> entities);

    T findById(ID id);

    List<T> findAll();

    boolean existsById(ID id);

    void deleteById(ID id);

    void delete(T entity);

    void deleteAll(Iterable<T> entities);

}
