package be.greenteam.lacuisine.service;


import be.greenteam.lacuisine.model.Rating;
import be.greenteam.lacuisine.repository.RatingRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.Collection;
import java.util.List;

@Service
@RequiredArgsConstructor
public class RatingServiceImpl implements RatingService {

    private final RatingRepository ratingRepo;

    @Override
    public Rating save(Rating entity) {
        return ratingRepo.save(entity);
    }

    @Override
    public List<Rating> saveAll(Collection<Rating> entities) {
        return ratingRepo.saveAll(entities);
    }

    @Override
    public Rating findById(Long aLong) {
        return ratingRepo.findById(aLong).orElseThrow(
                () -> new EntityNotFoundException("No Rating found for id:" + aLong)
        );
    }

    @Override
    public List<Rating> findAll() {
        return ratingRepo.findAll();
    }

    @Override
    public boolean existsById(Long aLong) {
        return ratingRepo.existsById(aLong);
    }

    @Override
    public void deleteById(Long aLong) {
        ratingRepo.deleteById(aLong);
    }

    @Override
    public void delete(Rating entity) {
        ratingRepo.delete(entity);
    }

    @Override
    public void deleteAll(Iterable<Rating> entities) {
        ratingRepo.deleteAll(entities);
    }

    @Override
    public List<Rating> findAllByRestaurantId(Long restaurantId) {
        return ratingRepo.findAllByRestaurantId(restaurantId);
    }

    @Override
    public List<Rating> findAllByUserId(Long userId) {
        return ratingRepo.findAllByUserId(userId);
    }
}
