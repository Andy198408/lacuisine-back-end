package be.greenteam.lacuisine.service;

import be.greenteam.lacuisine.model.RestaurantTable;
import be.greenteam.lacuisine.repository.RestaurantTableRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.Collection;
import java.util.List;

/**
 * Created by "Andy Van Camp" on 18/08/2021.
 */

@Service
@RequiredArgsConstructor
public class RestaurantTableServiceImpl implements RestaurantTableService {

    private final RestaurantTableRepository restaurantTableRepository;

    @Override
    public RestaurantTable save(RestaurantTable entity) {
        return restaurantTableRepository.save(entity);
    }

    @Override
    public List<RestaurantTable> saveAll(Collection<RestaurantTable> entities) {
        return restaurantTableRepository.saveAll(entities);
    }

    @Override
    public RestaurantTable findById(Long aLong) {
        return restaurantTableRepository.findById(aLong).orElseThrow(() -> new EntityNotFoundException("No entity was found for id: " + aLong));
    }

    @Override
    public List<RestaurantTable> findAll() {
        return restaurantTableRepository.findAll();
    }

    @Override
    public List<RestaurantTable> findAllByRestaurantId(Long id) {
        return restaurantTableRepository.findAllByRestaurantId(id);
    }

    @Override
    public List<RestaurantTable> findAllByRestaurantIdAndCapacity(Long id, int capacity) {
        return restaurantTableRepository.findAllByRestaurantIdAndCapacity(id, capacity);
    }

    @Override
    public boolean existsById(Long aLong) {
        return restaurantTableRepository.existsById(aLong);
    }

    @Override
    public void deleteById(Long aLong) {
        restaurantTableRepository.deleteById(aLong);
    }

    @Override
    public void delete(RestaurantTable entity) {
        restaurantTableRepository.delete(entity);
    }

    @Override
    public void deleteAll(Iterable<RestaurantTable> entities) {
        restaurantTableRepository.deleteAll(entities);
    }
}
