package be.greenteam.lacuisine.service;

import be.greenteam.lacuisine.model.Restaurant;
import be.greenteam.lacuisine.repository.RestaurantRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.Collection;
import java.util.List;

@Service
@RequiredArgsConstructor
public class RestaurantServiceImpl implements RestaurantService {

    private final RestaurantRepository restaurantRepo;

    @Override
    public Restaurant save(Restaurant entity) {
        return restaurantRepo.save(entity);
    }

    @Override
    public List<Restaurant> saveAll(Collection<Restaurant> entities) {
        return restaurantRepo.saveAll(entities);
    }

    @Override
    public Restaurant findById(Long aLong) {
        return restaurantRepo.findById(aLong).orElseThrow(() -> new EntityNotFoundException("There was no restaurant found for id: " + aLong));
    }

    @Override
    public List<Restaurant> findAll() {
        return restaurantRepo.findAll();
    }

    @Override
    public boolean existsById(Long aLong) {
        return restaurantRepo.existsById(aLong);
    }

    @Override
    public void deleteById(Long aLong) {
        restaurantRepo.deleteById(aLong);
    }

    @Override
    public void delete(Restaurant entity) {
        restaurantRepo.delete(entity);
    }

    @Override
    public void deleteAll(Iterable<Restaurant> entities) {
        restaurantRepo.deleteAll(entities);
    }

    @Override
    public List<Restaurant> findAllByOwnerId(Long id) {
        return restaurantRepo.findAllByOwnerId(id);
    }
}
