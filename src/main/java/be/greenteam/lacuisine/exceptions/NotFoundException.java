package be.greenteam.lacuisine.exceptions;

/**
 * Created by "Andy Van Camp" on 9/08/2021.
 */
public class NotFoundException extends RuntimeException {

    public NotFoundException() {
        super();
    }

    public NotFoundException(String message) {
        super(message);
    }

    public NotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
