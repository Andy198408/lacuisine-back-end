package be.greenteam.lacuisine.exceptions;

/**
 * Created by "Andy Van Camp" on 20/08/2021.
 */
public class ReservationException extends RuntimeException {

    public ReservationException() {
        super();
    }

    public ReservationException(String message) {
        super(message);
    }

    public ReservationException(String message, Throwable cause) {
        super(message, cause);
    }
}
