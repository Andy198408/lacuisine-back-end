package be.greenteam.lacuisine.exceptions;

/**
 * Created by "Andy Van Camp" on 9/08/2021.
 */
public class AllreadyExistException extends RuntimeException {

    public AllreadyExistException() {
        super();
    }

    public AllreadyExistException(String message) {
        super(message);
    }

    public AllreadyExistException(String message, Throwable cause) {
        super(message, cause);
    }
}
