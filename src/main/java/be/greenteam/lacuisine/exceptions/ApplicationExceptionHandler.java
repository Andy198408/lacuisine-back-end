package be.greenteam.lacuisine.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * Created by "Andy Van Camp" on 9/08/2021.
 */
@ControllerAdvice
public class ApplicationExceptionHandler {

    @ExceptionHandler(WalletInsufficientFundException.class)
    public ResponseEntity<Error> handleException(WalletInsufficientFundException e) {
        Error error = new Error(HttpStatus.I_AM_A_TEAPOT, e.getLocalizedMessage());
        return new ResponseEntity<>(error, error.getHttpStatus());
    }

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<Error> handleException(NotFoundException e) {
        Error error = new Error(HttpStatus.NOT_FOUND, e.getLocalizedMessage());
        return new ResponseEntity<>(error, error.getHttpStatus());
    }

    @ExceptionHandler(AllreadyExistException.class)
    public ResponseEntity<Error> handleException(AllreadyExistException e) {
        Error error = new Error(HttpStatus.BAD_REQUEST, e.getLocalizedMessage());
        return new ResponseEntity<>(error, error.getHttpStatus());
    }

    @ExceptionHandler(ReservationException.class)
    public ResponseEntity<Error> handleException(ReservationException e) {
        Error error = new Error(HttpStatus.BAD_REQUEST, e.getLocalizedMessage());
        return new ResponseEntity<>(error, error.getHttpStatus());
    }
}
