package be.greenteam.lacuisine.payload.map;

import be.greenteam.lacuisine.payload.model.MenuItemData;
import lombok.*;

import java.util.List;

/**
 * Created by "Andy Van Camp" on 19/12/2021.
 */

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class MenuItemMapLine {
    String name;
    List<MenuItemData> series;
}
