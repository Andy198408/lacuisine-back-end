package be.greenteam.lacuisine.payload.collection;

import be.greenteam.lacuisine.model.MenuItem;
import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MenuItemCollection {
    private List<MenuItem> menuItemCollection;
}
