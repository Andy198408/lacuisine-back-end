package be.greenteam.lacuisine.payload.collection;

import be.greenteam.lacuisine.model.Wallet;
import lombok.*;

import java.util.List;

/**
 * Created by "Andy Van Camp" on 9/08/2021.
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WalletCollection {
    private List<Wallet> walletCollection;
}
