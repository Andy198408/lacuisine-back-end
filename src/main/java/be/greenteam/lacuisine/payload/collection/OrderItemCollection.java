package be.greenteam.lacuisine.payload.collection;

import be.greenteam.lacuisine.model.OrderItem;
import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderItemCollection {
    private List<OrderItem> orderItemCollection;
}
