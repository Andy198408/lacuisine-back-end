package be.greenteam.lacuisine.payload.collection;

import be.greenteam.lacuisine.model.Advertisement;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by "Andy Van Camp" on 16/08/2021.
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AdvertisementCollection {
    private List<Advertisement> advertisementCollection;
}
