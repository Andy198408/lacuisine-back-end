package be.greenteam.lacuisine.payload.collection;

import be.greenteam.lacuisine.model.Rating;
import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RatingCollection {
    private List<Rating> ratingCollection;
}
