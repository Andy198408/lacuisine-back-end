package be.greenteam.lacuisine.payload.collection;

import be.greenteam.lacuisine.model.Reservation;
import lombok.*;

import java.util.List;

/**
 * Created by "Andy Van Camp" on 20/08/2021.
 */

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ReservationCollection {
    List<Reservation> reservationCollection;
}
