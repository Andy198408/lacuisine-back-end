package be.greenteam.lacuisine.payload.collection;

import be.greenteam.lacuisine.model.Order;
import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderCollection {
    private List<Order> orderCollection;
}
