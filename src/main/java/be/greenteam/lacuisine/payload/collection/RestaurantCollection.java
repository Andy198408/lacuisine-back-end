package be.greenteam.lacuisine.payload.collection;

import be.greenteam.lacuisine.model.Restaurant;
import lombok.*;

import java.util.List;

/**
 * Created by "Andy Van Camp" on 9/08/2021.
 */

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RestaurantCollection {
    private List<Restaurant> restaurantCollection;
}
