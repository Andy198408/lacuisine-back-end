package be.greenteam.lacuisine.payload.model;

import be.greenteam.lacuisine.model.FoodType;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class MenuItemDataAdv {
    private String name;
    private FoodType foodType;
    private Boolean vegetarian;
    private Integer amount;
    private Double price;
    private LocalDateTime orderDate;
}
