package be.greenteam.lacuisine.controller;

import be.greenteam.lacuisine.exceptions.AllreadyExistException;
import be.greenteam.lacuisine.exceptions.NotFoundException;
import be.greenteam.lacuisine.model.Address;
import be.greenteam.lacuisine.model.Restaurant;
import be.greenteam.lacuisine.payload.collection.AddressCollection;
import be.greenteam.lacuisine.service.AddressService;
import be.greenteam.lacuisine.service.RestaurantService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * Created by "Andy Van Camp" on 12/08/2021.
 */

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/address")
@Slf4j
@RequiredArgsConstructor
public class AddressController {

    private final AddressService addressService;
    private final RestaurantService restaurantService;

    @GetMapping("/{id:^\\d+$}")
    public ResponseEntity<Address> findAddressById(@PathVariable Long id) {
        if (!addressService.existsById(id)) {
            throw new NotFoundException("No Address found for: " + id);
        }
        return ResponseEntity.ok(addressService.findById(id));
    }

    @GetMapping
    public ResponseEntity<AddressCollection> findAllAddress() {
        return ResponseEntity.ok(AddressCollection.builder().addressCollection(addressService.findAll()).build());
    }

    @PostMapping
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<Address> saveAddress(@RequestBody Address address) {
        if (address.getId() != null) {
            throw new AllreadyExistException("New Address should not have an id. Received id: " + address.getId());
        }
        Restaurant foundResto = restaurantService.findById(address.getRestaurant().getId());
        address.setRestaurant(foundResto);
        return ResponseEntity.ok(addressService.save(address));
    }

    @PutMapping("/{id:^\\d+$}")
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<Address> updateAddress(@RequestBody Address address) {
        if (address.getId() == null) {
            return ResponseEntity.badRequest().build();
        }
        if (!addressService.existsById(address.getId())) {
            throw new NotFoundException("No Address exists for id: " + address.getId());
        }
        log.debug("address to update: " + address.getStreet());

        return ResponseEntity.ok(addressService.save(address));
    }

    @DeleteMapping("/{id:^\\d+$}")
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> deleteAddressById(@PathVariable Long id) {
        if (!addressService.existsById(id)) {
            throw new NotFoundException("No Address exists for id: " + id);
        }
        addressService.deleteById(id);
        return ResponseEntity.ok().build();
    }
}
