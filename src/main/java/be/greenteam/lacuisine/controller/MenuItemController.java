package be.greenteam.lacuisine.controller;

import be.greenteam.lacuisine.exceptions.NotFoundException;
import be.greenteam.lacuisine.model.MenuItem;
import be.greenteam.lacuisine.payload.collection.MenuItemCollection;
import be.greenteam.lacuisine.payload.map.MenuItemMapLine;
import be.greenteam.lacuisine.payload.model.MenuItemData;
import be.greenteam.lacuisine.service.MenuItemService;
import be.greenteam.lacuisine.service.RestaurantService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by "Dorien en Nick" on 12/08/2021.
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/menuitem")
@Slf4j
@RequiredArgsConstructor
public class MenuItemController {

    private final MenuItemService menuItemService;
    private final RestaurantService restaurantService;


    @GetMapping("/{id:^\\d+$}")
    public ResponseEntity<MenuItem> findMenuItemById(@PathVariable Long id) {
        return ResponseEntity.ok(menuItemService.findById(id));
    }

    @GetMapping
    public ResponseEntity<MenuItemCollection> findAllMenuItemsByMenu(@RequestParam(value = "menuId", required = false) Long menuId) {
        if (menuId != null) {
            return ResponseEntity.ok(MenuItemCollection.builder().menuItemCollection(menuItemService.findAllByMenuId(menuId)).build());
        }
        return ResponseEntity.ok(MenuItemCollection.builder().menuItemCollection(menuItemService.findAll()).build());
    }

    @GetMapping("/linegraph/{restaurantId:^\\d+$}")
    public ResponseEntity<List<MenuItemMapLine>> getLineGraphDataByRestaurantId(@PathVariable Long restaurantId) {
        if (restaurantId != null) {
            if (!restaurantService.existsById(restaurantId)) {
                throw new NotFoundException("No restaurant was found for id; " + restaurantId);
            }
            return ResponseEntity.ok(menuItemService.getLineGraphData(restaurantId));
        } else {
            throw new NotFoundException("No restaurant exists for id: " + restaurantId);
        }
    }

    @GetMapping("/piechart/{restaurantId:^\\d+$}")
    public ResponseEntity<List<MenuItemData>> getPieGraphDataByRestaurantId(@PathVariable Long restaurantId, @RequestParam(value = "type", required = false) String type) {
        if (restaurantId != null) {
            if (!restaurantService.existsById(restaurantId)) {
                throw new NotFoundException("No restaurant was found for id; " + restaurantId);
            }
            return ResponseEntity.ok(menuItemService.getPieChartGraphData(restaurantId, type));
        } else {
            throw new NotFoundException("No restaurant exists for id: " + restaurantId);
        }
    }

    @PostMapping
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<List<MenuItem>> saveMenuItem(@RequestBody List<MenuItem> menuItem) {
        return ResponseEntity.ok(menuItemService.saveAll(menuItem));
    }

    @PutMapping("/{id:^\\d+$}")
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> updateMenuItem(@RequestBody MenuItem menuItem) {
        if (menuItem.getId() == null) {
            return ResponseEntity.badRequest().build();
        }
        if (menuItem.getId() != null && !menuItemService.existsById(menuItem.getId())) {
            throw new NotFoundException("No menu item exists for id: " + menuItem.getId());
        }
        log.debug("menu item to update: " + menuItem.getId());
        return ResponseEntity.ok(menuItemService.save(menuItem));
    }

    @DeleteMapping("/{id:^\\d+$}")
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> deleteMenuItemById(@PathVariable Long id) {
        if (!menuItemService.existsById(id)) {
            throw new NotFoundException("No menu item exists for id: " + id);
        }
        menuItemService.deleteById(id);
        return ResponseEntity.ok().build();
    }
}


