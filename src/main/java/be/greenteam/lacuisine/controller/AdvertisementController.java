package be.greenteam.lacuisine.controller;

import be.greenteam.lacuisine.exceptions.AllreadyExistException;
import be.greenteam.lacuisine.exceptions.NotFoundException;
import be.greenteam.lacuisine.model.Advertisement;
import be.greenteam.lacuisine.model.Restaurant;
import be.greenteam.lacuisine.payload.collection.AdvertisementCollection;
import be.greenteam.lacuisine.service.AdvertisementService;
import be.greenteam.lacuisine.service.RestaurantService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

/**
 * Created by "Andy Van Camp" on 13/08/2021.
 */

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/advertisement")
@Slf4j
@RequiredArgsConstructor
public class AdvertisementController {

    private final AdvertisementService advertisementService;
    private final RestaurantService restaurantService;

    @GetMapping("/{id:^\\d+$")
    public ResponseEntity<Advertisement> findAdvertisementById(@PathVariable Long id) {
        if (!advertisementService.existsById(id)) {
            throw new NotFoundException("No advertisement found for id: " + id);
        }
        return ResponseEntity.ok(advertisementService.findById(id));
    }

    @GetMapping
    public ResponseEntity<AdvertisementCollection> findAllAdvertisement() {
        return ResponseEntity.ok(AdvertisementCollection.builder().advertisementCollection(advertisementService.findAll()).build());
    }

    @PostMapping
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<Advertisement> saveAdvertisement(@RequestParam(value = "cost") Double cost, @RequestBody Advertisement advertisement) {
        if (advertisement.getId() != null && advertisement.getRestaurant().getId() != null) {
            throw new AllreadyExistException("New Advertisement should not have an id. Received id: " + advertisement.getId());
        }
        Restaurant foundResto = restaurantService.findById(advertisement.getRestaurant().getId());
        if (cost != null) {
            long costLong = cost.longValue();
            log.info("cost :" + costLong);
            advertisement.setExpireDate(LocalDateTime.now().plusMinutes(costLong));
        }
        advertisement.setRestaurant(foundResto);
        return ResponseEntity.ok(advertisementService.save(advertisement));
    }

    @PutMapping("/{id:^\\d+$}")
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<Advertisement> updateAdvertisement(@RequestParam(value = "cost", required = false) Double cost, @RequestBody Advertisement advertisement) {
        if (advertisement.getId() == null) {
            return ResponseEntity.badRequest().build();
        }
        if (!advertisementService.existsById(advertisement.getId())) {
            throw new NotFoundException("No Advertisement found for id: " + advertisement.getId());
        }
        log.debug("Advertisement to update: " + advertisement.getId());
        if (cost != null) {
            long costLong = cost.longValue();
            log.info("update :" + costLong);
            log.info("expire :" + advertisement.getExpireDate());
            log.info("expire new :" + advertisement.getExpireDate().plusMinutes(costLong));
            advertisement.setExpireDate(advertisement.getExpireDate().plusMinutes(costLong));
        }
        return ResponseEntity.ok(advertisementService.save(advertisement));
    }

    @PutMapping
    public ResponseEntity<AdvertisementCollection> updateAdvertisement(@RequestBody AdvertisementCollection advertisementCollection) {
        return ResponseEntity.ok(AdvertisementCollection.builder().advertisementCollection(advertisementService.saveAll(advertisementCollection.getAdvertisementCollection())).build());
    }

    @DeleteMapping("/{id:^\\d+$}")
    public ResponseEntity<?> deleteById(@PathVariable Long id) {
        if (!advertisementService.existsById(id)) {
            throw new NotFoundException("No Advertisement found for id: " + id);
        }
        advertisementService.deleteById(id);
        return ResponseEntity.ok().build();
    }


}
