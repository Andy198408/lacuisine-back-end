package be.greenteam.lacuisine.controller;

import be.greenteam.lacuisine.exceptions.AllreadyExistException;
import be.greenteam.lacuisine.exceptions.NotFoundException;
import be.greenteam.lacuisine.exceptions.ReservationException;
import be.greenteam.lacuisine.model.Openinghours;
import be.greenteam.lacuisine.model.Reservation;
import be.greenteam.lacuisine.model.RestaurantTable;
import be.greenteam.lacuisine.payload.collection.ReservationCollection;
import be.greenteam.lacuisine.service.*;
import be.greenteam.lacuisine.util.pdf.ReservationListPDFExporter;
import be.greenteam.lacuisine.util.pdf.ReservationPdf;
import be.greenteam.lacuisine.util.pdf.payload.ReservationRequest;
import be.greenteam.lacuisine.util.reservation.GenerateTimeslot;
import be.greenteam.lacuisine.util.reservation.Timeslot;
import be.greenteam.lacuisine.util.reservation.payload.request.TimeslotRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;

/**
 * Created by "Andy Van Camp" on 20/08/2021.
 */

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/reservation")
@Slf4j
@RequiredArgsConstructor
public class ReservationController {

    private final RestaurantService restaurantService;
    private final UserService userService;
    private final ReservationService reservationService;
    private final RestaurantTableService restaurantTableService;
    private final OpeninghoursService openinghoursService;
    private final ReservationPdf reservationPdf;
    private final ReservationListPDFExporter reservationListPDFExporter;

    @GetMapping("/{id:^\\d+$}")
    public ResponseEntity<Reservation> findReservationById(@PathVariable Long id) {
        return ResponseEntity.ok(reservationService.findById(id));
    }

    @GetMapping()
    public ResponseEntity<ReservationCollection> findAllReservation(
            @RequestParam(value = "userId", required = false) Long userId,
            @RequestParam(value = "restaurantId", required = false) Long restaurantId) {
        if (userId != null) {
            if (!userService.existsById(userId)) {
                throw new NotFoundException("No user found for id: " + userId);
            }
            return ResponseEntity.ok(ReservationCollection.builder().reservationCollection(reservationService.findAllByUserId(userId)).build());
        }
        if (restaurantId != null) {
            if (!restaurantService.existsById(restaurantId)) {
                throw new NotFoundException("No restaurant found for id: " + restaurantId);
            }
            return ResponseEntity.ok(ReservationCollection.builder().reservationCollection(reservationService.findAllByRestaurantId(restaurantId)).build());
        }

        return ResponseEntity.ok(
                ReservationCollection.builder().reservationCollection(reservationService.findAll()).build()
        );
    }

    @PostMapping("/timeslots")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<HashMap<Long, List<Timeslot>>> generateTimeslots(@RequestBody TimeslotRequest timeslotRequest) {
        if (!restaurantService.existsById(timeslotRequest.getRestaurant().getId())) {
            throw new NotFoundException("No restaurant found for id: " + timeslotRequest.getRestaurant().getId());
        }
        if (timeslotRequest.getReservationDate().isBefore(LocalDate.now())) {
            throw new ReservationException("Can't go back in time, and even then I would not know you have made a " +
                    "reservation!");
        }
        if (timeslotRequest.getPartySize() <= 0) {
            throw new ReservationException("Party size must be larger then 0");
        }
        List<RestaurantTable> tables = restaurantTableService.findAllByRestaurantIdAndCapacity(
                timeslotRequest.getRestaurant().getId(), timeslotRequest.getPartySize());

        HashMap<Long, List<Reservation>> tableMap = new HashMap<>();
        tables.forEach(table -> {
            tableMap.put(table.getId(), reservationService.findAllByRestaurantTableIdAndDate(table.getId(), timeslotRequest.getReservationDate()));
        });

        List<Openinghours> openinghours = openinghoursService.findAllByRestaurantIdAndWeekday(
                timeslotRequest.getRestaurant().getId(), timeslotRequest.getReservationDate().getDayOfWeek());

        GenerateTimeslot generateTimeslot = new GenerateTimeslot();
        HashMap<Long, List<Timeslot>> timeslotMap = new HashMap<>();
        tableMap.forEach((k, reservations) -> {
            timeslotMap.put(k, generateTimeslot.generateAvailableTimeslot(openinghours, reservations, timeslotRequest.getReservationDate()));
        });

        return ResponseEntity.ok(timeslotMap);
    }

    @PostMapping
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<Reservation> saveReservation(@RequestBody Reservation reservation) {
        if (reservation.getId() != null) {
            throw new AllreadyExistException("New reservation should not have an id. Received id: " + reservation.getId());
        }
        if (reservation.getRestaurant() == null || reservation.getUser() == null) {
            throw new ReservationException("There was no Restaurant or user given for reservation!");
        }
        if (reservation.getRestaurant().getId() == null || reservation.getUser().getId() == null) {
            throw new ReservationException(
                    "No id exception: userId: " + reservation.getUser().getId() +
                            "\n restaurantId: " + reservation.getRestaurant().getId());
        }
        log.debug("reservation to save: " + reservation.getId());
        return ResponseEntity.ok(reservationService.save(reservation));

    }

    @DeleteMapping("/{id:^\\d+$}")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> deleteReservationById(@PathVariable Long id) {
        if (!reservationService.existsById(id)) {
            throw new NotFoundException("No Reservation exists for id: " + id);
        }
        reservationService.deleteById(id);
        return ResponseEntity.ok().build();
    }


    @GetMapping("/reservationPdf/{id:^\\d+$}")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<byte[]> getReservationPdf(@PathVariable Long id) {
        if (!reservationService.existsById(id)) {
            throw new NotFoundException("No Reservation exists for id: " + id);
        }

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        OutputStream reservationPdf = this.reservationPdf.createReservationPdf(outputStream, id);
        ByteArrayOutputStream reservationPdfByte = (ByteArrayOutputStream) reservationPdf;

        byte[] pdfBytes = reservationPdfByte.toByteArray();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_PDF);
        // Here you have to set the actual filename of your pdf
        String filename = "reservation.pdf";
        headers.setContentDispositionFormData(filename, filename);
        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
        ResponseEntity<byte[]> response = new ResponseEntity<>(pdfBytes, headers, HttpStatus.OK);
        return response;
    }

    @PostMapping("/reservationPdfList/{restaurantId:\\d+$}")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<byte[]> getReservationListByIdBetweenDates(@PathVariable Long restaurantId, @RequestBody ReservationRequest reservationRequest) {
        if (!restaurantService.existsById(restaurantId)) {
            throw new NotFoundException("No restaurant found for id: " + restaurantId);
        }
        List<Reservation> allByRestaurantId = reservationService.findAllByRestaurantId(restaurantId);
        log.debug("Reservations controller: " + allByRestaurantId);

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        OutputStream listPdfReservations = reservationListPDFExporter.createReservationListBetweenZip(reservationRequest.getStart(), reservationRequest.getEnd(), allByRestaurantId, outputStream);
        ByteArrayOutputStream listPdfReservationsByte = (ByteArrayOutputStream) listPdfReservations;

        final byte[] listPdfbytes = listPdfReservationsByte.toByteArray();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        // Here you have to set the actual filename of your zip
        String filename = "reservations.zip";
        headers.setContentDispositionFormData(filename, filename);
        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
        ResponseEntity<byte[]> response = new ResponseEntity<>(listPdfbytes, headers, HttpStatus.OK);
        return response;
    }

    @GetMapping("/checkreservation/{id:^\\d+$}")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public Boolean checkReservation(@PathVariable Long id) {
        return (!reservationService.existsById(id));
    }

}
