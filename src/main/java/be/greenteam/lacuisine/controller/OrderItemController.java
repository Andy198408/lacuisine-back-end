package be.greenteam.lacuisine.controller;

import be.greenteam.lacuisine.exceptions.Error;
import be.greenteam.lacuisine.exceptions.NotFoundException;
import be.greenteam.lacuisine.model.OrderItem;
import be.greenteam.lacuisine.payload.collection.OrderItemCollection;
import be.greenteam.lacuisine.service.OrderItemService;
import be.greenteam.lacuisine.service.OrderService;
import be.greenteam.lacuisine.service.RestaurantService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/orderitem")
@Slf4j
@RequiredArgsConstructor
public class OrderItemController {
    private final OrderItemService orderItemService;
    private final OrderService orderService;
    private final RestaurantService restaurantService;

    @GetMapping("/{id:^\\d+$}")
    public ResponseEntity<OrderItem> findOrderItemById(@PathVariable Long id) {
        return ResponseEntity.ok(orderItemService.findById(id));
    }

    @GetMapping()
    public ResponseEntity<OrderItemCollection> findAllOrderItems(@RequestParam(value = "orderItemId", required = false) Long orderItemId) {
        if (orderItemId != null) {
            if (!orderService.existsById(orderItemId)) {
                throw new NotFoundException("No Order found for id: " + orderItemId);
            }
            return ResponseEntity.ok(OrderItemCollection.builder().orderItemCollection(orderItemService.findOrderItemByOrderId(orderItemId)).build());
        }
        return ResponseEntity.ok(OrderItemCollection.builder().orderItemCollection(orderItemService.findAll()).build());
    }

    @PostMapping
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN') or hasRole('USER')")
    public ResponseEntity<OrderItemCollection> saveAllOrderItems(@RequestBody OrderItemCollection orderItemCollection) throws Error {
        if (orderItemCollection.getOrderItemCollection().size() == 0) {
            throw new Error(HttpStatus.I_AM_A_TEAPOT, "OrderItemList Is empty");
        }
        log.debug("Orderitems to save: " + orderItemCollection.getOrderItemCollection());
        List<OrderItem> saveOrderItemList = orderItemService.saveAll(orderItemCollection.getOrderItemCollection());
        return ResponseEntity.ok(OrderItemCollection.builder().orderItemCollection(saveOrderItemList).build());
    }

    @DeleteMapping("/{id:^\\d+$}")
    public ResponseEntity<?> deleteOrderItemById(@PathVariable Long id) {
        if (!orderItemService.existsById(id)) {
            throw new NotFoundException("No OrderItem exists for id: " + id);
        }
        orderItemService.deleteById(id);
        return ResponseEntity.ok().build();
    }

    @PutMapping
    public ResponseEntity<?> updateOrderItem(@RequestBody OrderItemCollection orderItemCollection) {
        if (orderItemCollection.getOrderItemCollection().size() == 0) {
            return ResponseEntity.badRequest().build();
        }
        log.debug("OrderItems to update " + orderItemCollection.getOrderItemCollection());
        return ResponseEntity.ok(orderItemService.saveAll(orderItemCollection.getOrderItemCollection()));
    }


}
