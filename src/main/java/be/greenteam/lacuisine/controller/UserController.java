package be.greenteam.lacuisine.controller;

import be.greenteam.lacuisine.exceptions.NotFoundException;
import be.greenteam.lacuisine.model.User;
import be.greenteam.lacuisine.payload.collection.UserCollection;
import be.greenteam.lacuisine.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

/**
 * Created by "Andy Van Camp" on 9/08/2021.
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/user")
@Slf4j
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    private final PasswordEncoder encoder;

    @GetMapping("/{id:^\\d+$}")
    public ResponseEntity<User> findUserById(@PathVariable Long id) {
        return ResponseEntity.ok(userService.findById(id));
    }

    @GetMapping()
    public ResponseEntity<UserCollection> findAllUsers() {
        return ResponseEntity.ok(UserCollection.builder().userCollection(userService.findAll()).build());
    }

    @DeleteMapping("/{id:^\\d+$}")
    @PreAuthorize(("hasRole('USER') or hasRole('ADMIN')"))
    public ResponseEntity<?> deleteUserById(@PathVariable Long id) {
        if (!userService.existsById(id)) {
            throw new NotFoundException("No user exists for id: " + id);
        }
        userService.deleteById(id);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/update")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<User> updateUser(@RequestBody User user) {
        if (Objects.isNull(user.getId())) {
            throw new NotFoundException("Attempt to update nonexistent user");
        }
        if (!userService.existsById(user.getId())) {
            throw new NotFoundException("No user exists for id: " + user.getId());
        }

        user.setPassword(encoder.encode(user.getPassword()));

        return ResponseEntity.ok(userService.save(user));
    }

}
