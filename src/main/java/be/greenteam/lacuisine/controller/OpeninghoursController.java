package be.greenteam.lacuisine.controller;

import be.greenteam.lacuisine.exceptions.AllreadyExistException;
import be.greenteam.lacuisine.exceptions.NotFoundException;
import be.greenteam.lacuisine.model.Openinghours;
import be.greenteam.lacuisine.model.Restaurant;
import be.greenteam.lacuisine.payload.collection.OpeninghoursCollection;
import be.greenteam.lacuisine.service.OpeninghoursService;
import be.greenteam.lacuisine.service.RestaurantService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Created by "Andy Van Camp" on 12/08/2021.
 */

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/openinghours")
@Slf4j
@RequiredArgsConstructor
public class OpeninghoursController {

    private final OpeninghoursService openinghoursService;
    private final RestaurantService restaurantService;

    @GetMapping("/{id:^\\d+$}")
    public ResponseEntity<Openinghours> findOpeninghoursById(@PathVariable Long id) {
        if (!openinghoursService.existsById(id)) {
            throw new NotFoundException("No Openinghours found for: " + id);
        }
        return ResponseEntity.ok(openinghoursService.findById(id));
    }

    @GetMapping
    public ResponseEntity<OpeninghoursCollection> findAllOpeninghours(@RequestParam(value = "restaurantId", required = false) Long restaurantId) {
        if (restaurantId != null) {
            if (!restaurantService.existsById(restaurantId)) {
                throw new NotFoundException("No Restaurant found for id: " + restaurantId);
            }
            return ResponseEntity.ok(OpeninghoursCollection.builder().openinghoursCollection(openinghoursService.findAllByRestaurantId(restaurantId)).build());
        }
        return ResponseEntity.ok(OpeninghoursCollection.builder().openinghoursCollection(openinghoursService.findAll()).build());
    }

    @PostMapping
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<Openinghours> saveOpeninghours(@Valid @RequestBody Openinghours openinghours) {
        if (openinghours.getId() != null) {
            throw new AllreadyExistException("New Openinghours should not have an id. Received id: " + openinghours.getId());
        }
        Restaurant foundResto = restaurantService.findById(openinghours.getRestaurant().getId());
        openinghours.setRestaurant(foundResto);
        return ResponseEntity.ok(openinghoursService.save(openinghours));
    }

    @PutMapping
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<Openinghours> updateOpeninghours(@Valid @RequestBody Openinghours openinghours) {
        if (openinghours.getId() == null) {
            return ResponseEntity.badRequest().build();
        }
        if (!openinghoursService.existsById(openinghours.getId())) {
            throw new NotFoundException("No Openinghours exists for id: " + openinghours.getId());
        }
        log.debug("address to update: " + openinghours.getId());

        return ResponseEntity.ok(openinghoursService.save(openinghours));
    }

    @DeleteMapping("/{id:^\\d+$}")
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> deleteOpeninghoursById(@PathVariable Long id) {
        if (!openinghoursService.existsById(id)) {
            throw new NotFoundException("No Openinghours exists for id: " + id);
        }
        openinghoursService.deleteById(id);
        return ResponseEntity.ok().build();
    }
}
