package be.greenteam.lacuisine.controller;

import be.greenteam.lacuisine.exceptions.AllreadyExistException;
import be.greenteam.lacuisine.exceptions.NotFoundException;
import be.greenteam.lacuisine.model.Menu;
import be.greenteam.lacuisine.model.Restaurant;
import be.greenteam.lacuisine.payload.collection.RestaurantCollection;
import be.greenteam.lacuisine.service.MenuService;
import be.greenteam.lacuisine.service.RestaurantService;
import be.greenteam.lacuisine.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * Created by "Andy Van Camp" on 9/08/2021.
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/restaurant")
@Slf4j
@RequiredArgsConstructor
public class RestaurantController {

    private final RestaurantService restaurantService;
    private final UserService userService;
    private final MenuService menuService;

    @GetMapping("/{id:^\\d+$}")
    public ResponseEntity<Restaurant> findRestaurantById(@PathVariable Long id) {
        if (!restaurantService.existsById(id)){
            throw new NotFoundException("No restaurant found for id: " + id);
        }
        return ResponseEntity.ok(restaurantService.findById(id));
    }

    @GetMapping()
    public ResponseEntity<RestaurantCollection> findAllRestaurant(@RequestParam(value = "userId", required = false) Long userId) {

        if (userId != null) {
            if (!userService.existsById(userId)) {
                throw new NotFoundException("No user found for id: " + userId);
            }
            return ResponseEntity.ok(RestaurantCollection.builder().restaurantCollection(restaurantService.findAllByOwnerId(userId)).build());
        }

        return ResponseEntity.ok(
                RestaurantCollection.builder().restaurantCollection(restaurantService.findAll()).build()
        );
    }

    @PostMapping
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<Restaurant> saveRestaurant(@RequestBody Restaurant restaurant) {
        if (restaurant.getId() != null) {
            throw new AllreadyExistException("New restaurant should not have an id. Received id: " + restaurant.getId());
        }
        log.debug("restaurant to save: " + restaurant.getId());
        Restaurant saveRestaurant = restaurantService.save(restaurant);
        Menu newMenu = Menu.builder().name(saveRestaurant.getName()).restaurant(saveRestaurant).build();
        menuService.save(newMenu);

        return ResponseEntity.ok(saveRestaurant);
    }

    @PutMapping("/{id:^\\d+$}")
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> updateRestaurant(@RequestBody Restaurant restaurant) {
        if (restaurant.getId() == null) {
            return ResponseEntity.badRequest().build();
        }
        if (restaurant.getId() != null && !restaurantService.existsById(restaurant.getId())) {
            throw new NotFoundException("No Restaurant exists for id: " + restaurant.getId());
        }
        log.debug("restaurant to update: " + restaurant.getName());
        return ResponseEntity.ok(restaurantService.save(restaurant));
    }

    @DeleteMapping("/{id:^\\d+$}")
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<?> deleteRestaurantById(@PathVariable Long id) {
        if (!restaurantService.existsById(id)) {
            throw new NotFoundException("No Restaurant exists for id: " + id);
        }
        restaurantService.deleteById(id);
        return ResponseEntity.ok().build();
    }


}

