package be.greenteam.lacuisine.repository;
/**
 * Author: Andy.
 */

import be.greenteam.lacuisine.model.ERole;
import be.greenteam.lacuisine.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(ERole name);
}
