package be.greenteam.lacuisine.repository;

import be.greenteam.lacuisine.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OrderRepository extends JpaRepository<Order, Long> {
    List<Order> findAllByRestaurantIdOrderByOrderTimeDesc(Long restaurantId);
    List<Order> findAllByUserIdOrderByOrderTimeDesc(Long userId);
}
