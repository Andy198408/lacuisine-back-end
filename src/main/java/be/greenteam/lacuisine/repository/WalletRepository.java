package be.greenteam.lacuisine.repository;

import be.greenteam.lacuisine.model.Wallet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by "Andy Van Camp" on 9/08/2021.
 */

@Repository
public interface WalletRepository extends JpaRepository<Wallet, Long> {

    @Transactional
    @Modifying
    @Query("update Wallet set storeCredit = storeCredit + :value where user.id = :id")
    void addingToStoreCreditById(float value, Long id);

    @Query("select w.storeCredit from Wallet w where w.id = :id")
    float findValueStoreCredit(Long id);


}
