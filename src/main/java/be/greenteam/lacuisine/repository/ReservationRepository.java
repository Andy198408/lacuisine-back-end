package be.greenteam.lacuisine.repository;

import be.greenteam.lacuisine.model.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface ReservationRepository extends JpaRepository<Reservation, Long> {
    List<Reservation> findAllByUserId(Long id);
    List<Reservation> findAllByRestaurantId(Long id);
    List<Reservation> findAllByRestaurantTableId(Long id);
    List<Reservation> findAllByRestaurantTableIdAndDate(Long id, LocalDate date);
}
