package be.greenteam.lacuisine.repository;

import be.greenteam.lacuisine.model.Menu;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MenuRepository extends JpaRepository<Menu, Long> {
}
