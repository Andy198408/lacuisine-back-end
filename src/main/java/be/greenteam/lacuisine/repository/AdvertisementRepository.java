package be.greenteam.lacuisine.repository;
/**
 * Created by "Andy Van Camp" on 13/08/2021.
 */

import be.greenteam.lacuisine.model.Advertisement;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;

public interface AdvertisementRepository extends JpaRepository<Advertisement, Long> {
    List<Advertisement> findAllByExpireDateBefore(LocalDateTime localDateTime);
}
