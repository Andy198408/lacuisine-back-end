package be.greenteam.lacuisine.repository;

import be.greenteam.lacuisine.model.Openinghours;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.DayOfWeek;
import java.util.List;

/**
 * Created by "Andy Van Camp" on 17/08/2021.
 */
public interface OpeninghoursRepository extends JpaRepository<Openinghours, Long> {
    List<Openinghours> findAllByRestaurantId(Long id);

    List<Openinghours> findAllByRestaurantIdAndWeekday(Long id, DayOfWeek weekday);
}
