package be.greenteam.lacuisine.util.pdf;

import java.io.OutputStream;

public interface ReservationPdf {
    OutputStream createReservationPdf(OutputStream outputStream, Long id);
}
