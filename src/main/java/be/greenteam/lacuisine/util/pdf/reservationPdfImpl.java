package be.greenteam.lacuisine.util.pdf;

import be.greenteam.lacuisine.model.Reservation;
import be.greenteam.lacuisine.service.ReservationService;
import be.greenteam.lacuisine.util.qrcode.QRCodeGenerator;
import com.google.zxing.WriterException;
import com.itextpdf.io.font.FontConstants;
import com.itextpdf.io.image.ImageData;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Image;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.TextAlignment;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.OutputStream;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RequiredArgsConstructor
@Slf4j
@Service
public class reservationPdfImpl extends BasicPDFStartKit implements ReservationPdf {

    private final ReservationService reservationService;

    public OutputStream createReservationPdf(OutputStream outputStream, Long id) {

        createPdfDoc(outputStream);

        writeTitle();

        writeTableData(id);

        addQrCode(id);

        return getPdf();
    }

    private void writeTitle() {
        Paragraph paragraphReservation = new Paragraph("Reservation");
        paragraphReservation.setTextAlignment(TextAlignment.CENTER);
        document.add(paragraphReservation);
    }

    private void addQrCode(Long id) {
        byte[] qrCodeImage = null;

        try {
            qrCodeImage = QRCodeGenerator.getQRCodeImage("http://localhost:8080/api/reservation/checkreservation/" + id, 200, 200);
        } catch (WriterException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (qrCodeImage != null && qrCodeImage.length > 0) {
            ImageData png = ImageDataFactory.createPng(qrCodeImage);
            Image image = new Image(png);
            document.add(image);
        }
    }

    private void writeTableData(Long id) {

        Reservation reservation = reservationService.findById(id);


        float[] pointColumnWidths = {150F, 150F};
        Table table = new Table(pointColumnWidths);

        PdfFont font = null;
        try {
            font = PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD);
        } catch (IOException e) {
            log.error("Problem creating font!");
            e.printStackTrace();
        }

        List<String> strings = Arrays.asList("Restaurant: ", "Klant naam: ", "Datum: ", "Aankomst: ", "Aantal personen: ");
        ArrayList<String> data = new ArrayList<>();
        data.add(reservation.getRestaurant().getName());
        data.add(reservation.getUser().getUsername());
        data.add(reservation.getDate().toString());
        data.add(reservation.getStartTime().truncatedTo(ChronoUnit.MINUTES).toString());
        data.add(String.valueOf(reservation.getPartySize()));

        PdfFont finalFont = font;

        strings.forEach(string -> {
            Cell first = new Cell();
            first.setBackgroundColor(com.itextpdf.kernel.color.Color.BLUE);
            first.setPadding(5);
            first.setFont(finalFont);
            first.add(string);
            table.addCell(first);
            Cell second = new Cell();
            second.setBackgroundColor(com.itextpdf.kernel.color.Color.BLUE);
            second.setPadding(5);
            second.setFont(finalFont);
            second.add(data.get(strings.indexOf(string)));
            table.addCell(second);
        });

        document.add(table);
    }


}
