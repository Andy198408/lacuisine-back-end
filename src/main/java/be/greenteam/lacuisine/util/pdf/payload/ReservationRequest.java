package be.greenteam.lacuisine.util.pdf.payload;

import lombok.Data;

import java.time.LocalDate;

/**
 * Created by "Andy Van Camp" on 13/11/2021.
 */
@Data
public class ReservationRequest {

        private LocalDate start;
        private LocalDate end;

}
