package be.greenteam.lacuisine.util.pdf;

import be.greenteam.lacuisine.model.Reservation;

import java.io.OutputStream;
import java.time.LocalDate;
import java.util.List;

/**
 * Created by "Andy Van Camp" on 13/11/2021.
 */
public interface ReservationListPDFExporter {
    OutputStream createReservationListBetweenZip(LocalDate start, LocalDate end, List<Reservation> allByRestaurantId, OutputStream outputStream);
}
