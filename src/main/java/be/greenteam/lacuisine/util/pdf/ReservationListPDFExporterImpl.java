package be.greenteam.lacuisine.util.pdf;

import be.greenteam.lacuisine.model.Reservation;
import be.greenteam.lacuisine.util.zip.ZipService;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfReader;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.utils.PdfMerger;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by "Andy Van Camp" on 13/11/2021.
 */
@RequiredArgsConstructor
@Slf4j
@Service
public class ReservationListPDFExporterImpl implements ReservationListPDFExporter {

    private final ReservationPdf reservationPdf;
    private final ZipService zipService;

    @Override
    public OutputStream createReservationListBetweenZip(LocalDate start, LocalDate end, List<Reservation> allByRestaurantId, OutputStream outputStream) {

        List<Reservation> reservations = allByRestaurantId.stream()
                .filter(reservation -> start.isBefore(reservation.getDate().minusDays(1))
                        && end.isAfter(reservation.getDate().minusDays(1))
                )
                .collect(Collectors.toList());

        Integer zipFileKey = null;
        Integer zipBackupKey = null;
        Integer zipMergeKey = zipService.createZipFile(outputStream);
        ;
        try {
            zipBackupKey = zipService.createZipFile(new FileOutputStream("C:\\Users\\andyv\\Downloads\\testMultipleStream.zip"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            zipFileKey = zipService.createZipFile(new FileOutputStream("C:\\Users\\andyv\\Downloads\\testPdfZipFile.zip"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        ByteArrayOutputStream mergedPDF = new ByteArrayOutputStream();
        PdfWriter pdfWriter = new PdfWriter(mergedPDF);
        PdfDocument pdfDoc = new PdfDocument(pdfWriter);
        PdfMerger pdfMerger = new PdfMerger(pdfDoc);


        log.debug("Reservations: " + reservations);
        Integer finalZipBackupKey = zipBackupKey;
        Integer finalZipFileKey = zipFileKey;
        reservations.forEach(reservation -> {
            final ByteArrayOutputStream reservationPdf = (ByteArrayOutputStream) this.reservationPdf.createReservationPdf(new ByteArrayOutputStream(), reservation.getId());
            zipService.addToZip(reservation.getUser().getUsername() + "_" + reservation.getDate() + "_created_" + LocalTime.now() + ".pdf", new ByteArrayInputStream(reservationPdf.toByteArray()), finalZipFileKey);
            // making extra zip as back-up
            if (finalZipBackupKey != null) {
                zipService.addToZip(reservation.getUser().getUsername() + "_" + reservation.getDate() + "_created_" + LocalTime.now() + ".pdf", new ByteArrayInputStream(reservationPdf.toByteArray()), finalZipBackupKey);
            }
            try {
                PdfDocument pdfDocument = new PdfDocument(new PdfReader(new ByteArrayInputStream(reservationPdf.toByteArray())));
                pdfMerger.merge(pdfDocument, 1, pdfDocument.getNumberOfPages());
                pdfDocument.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        pdfMerger.close();
        zipService.addToZip("mergedPdf.pdf", new ByteArrayInputStream(mergedPDF.toByteArray()), zipMergeKey);

        zipService.close(zipMergeKey);
        zipService.close(zipFileKey);
        zipService.close(zipBackupKey);
        return outputStream;

    }

}
