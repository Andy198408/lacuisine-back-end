package be.greenteam.lacuisine.util.zip;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StreamUtils;

import java.io.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/**
 * Created by "Andy Van Camp" on 19/11/2021.
 */

@Slf4j
@RequiredArgsConstructor
@Service
public class ZipServiceImpl implements ZipService {

    private final HashMap<Integer, ZipOutputStream> zipOutputStreamHashMap;
    private final HashMap<Integer, Set<String>> zipOutputStreamEntries;

    /**
     * This will create a new zipfile link it to the outputstream
     * and return the key to access it.
     *
     * @param outputStream - stream to add the zip files to
     * @return key - key matching ZipStream
     */
    @Override
    public Integer createZipFile(OutputStream outputStream) {
        final ZipOutputStream zipOutputStream = new ZipOutputStream(outputStream);

        Integer newKey = getNewKey(zipOutputStreamHashMap);

        zipOutputStreamHashMap.put(newKey, zipOutputStream);
        zipOutputStreamEntries.put(newKey, new HashSet<>());
        return newKey;
    }

    /**
     * This will create a new zip entry and
     * add a new {@link ByteArrayInputStream} to the zip stream
     * that matches with the key.
     *
     * @param zipEntry - Name of the file
     * @param input    - ByteArrayInputStream to add
     * @Param key - key matching ZipStream
     */
    @Override
    public void addToZip(String zipEntry, ByteArrayInputStream input, Integer key) {
        final ZipEntry zipEntryFinal = new ZipEntry(zipEntry);
        if (zipOutputStreamEntries.get(key).contains(zipEntry)) {
            log.error("Attempted to create a zip file with duplicate entries");
        } else {
            zipOutputStreamEntries.get(key).add(zipEntry);
            try {
                ZipOutputStream zipOutputStream = zipOutputStreamHashMap.get(key);
                zipOutputStream.putNextEntry(zipEntryFinal);
                StreamUtils.copy(input, zipOutputStream);
            } catch (IOException ex) {
                log.debug("Problem @ creating next entry.");
                ex.printStackTrace();
            }
        }
    }

    /**
     * This will close and remove the zip stream matching the key
     * this is necessary to create a valid zipFile.
     *
     * @Param key - key matching ZipStream
     */
    @Override
    public void close(Integer key) {
        try {
            ZipOutputStream zipOutputStream = zipOutputStreamHashMap.get(key);
            zipOutputStream.close();
            zipOutputStreamHashMap.remove(key);
            zipOutputStreamEntries.remove(key);
        } catch (IOException e) {
            log.error("Problem with closing the zipOutputStream.");
            e.printStackTrace();
        }
    }

    /**
     * @param input - A legal inputstream of a zip file
     * @return - set of entries
     * @throws IOException
     */
    @Override
    public Set<String> findAllEntriesZipFile(InputStream input) throws IOException {
        ZipInputStream zis = new ZipInputStream(input);
        System.out.println(zis);
        ZipEntry nextEntry;
        HashSet<String> entries = new HashSet<>();
        while ((nextEntry = zis.getNextEntry()) != null) {
            System.out.println(nextEntry.getName());
            if (entries.contains(nextEntry.getName())) {
                throw new RuntimeException("Duplicate entries detected in stream");
            }
            entries.add(nextEntry.getName());
        }
        return entries;
    }

    /**
     * This method creates a new ByteArrayOutputStream, extracts the wanted entries from input stream.
     * These then get written to the new ByteArrayOutputStream that then gets returned.
     *
     * @param entries set of string filenames that need to be extracted.
     * @param input   a valid zip input stream
     * @return ByteArrayOutputStream containing a zip ByteArray.
     * @throws IOException
     */
    @Override
    public ByteArrayOutputStream createNewZipOfEntries(Set entries, InputStream input) throws IOException {
        ZipInputStream zis = new ZipInputStream(input);
        System.out.println(zis);
        ZipEntry nextEntry;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        Integer zipKey = createZipFile(byteArrayOutputStream);
        while ((nextEntry = zis.getNextEntry()) != null) {

            if (entries.contains(nextEntry.getName())) {
                System.out.println(nextEntry.getName());
                addToZip(nextEntry.getName(), new ByteArrayInputStream(zis.readAllBytes()), zipKey);
            }
        }
        close(zipKey);
        return byteArrayOutputStream;
    }

    /**
     * This method writes output stream to a file.
     *
     * @param file                  where the output stream need to be written to
     * @param byteArrayOutputStream stream containing the data.
     * @throws IOException
     */
    @Override
    public void writeByteArrayToFile(File file, ByteArrayOutputStream byteArrayOutputStream) throws IOException {
        ByteArrayInputStream zipInput = new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
        FileOutputStream fos = new FileOutputStream(file);
        StreamUtils.copy(zipInput, fos);
        fos.close();
    }

    /**
     * Extracts and writes files from a zip to chosen location.
     *
     * @param entries       entries from the zip to be extracted
     * @param input         a valid input stream from zip file
     * @param filedirectory location to put the files
     * @throws IOException
     */
    @Override
    public void extractFilesFromZip(Set entries, InputStream input, File filedirectory) throws IOException {
        if (!filedirectory.isDirectory()) {
            throw new RuntimeException("Given filedirectory is not a directory or does not exist");
        }
        ZipInputStream zis = new ZipInputStream(input);
        System.out.println(zis);
        ZipEntry nextEntry;
        while ((nextEntry = zis.getNextEntry()) != null) {

            if (entries.contains(nextEntry.getName())) {
                System.out.println(nextEntry.getName());

                FileOutputStream fileOutputStream = new FileOutputStream(filedirectory.getAbsolutePath() + "/" + nextEntry.getName().replaceAll("[*:/\\\\?|<>\"]", "-"));
                fileOutputStream.write(zis.readAllBytes());
                fileOutputStream.close();
            }
        }
        zis.close();
    }

    private Integer getNewKey(Map output) {
        final Set<Integer> keySet = output.keySet();
        Integer newKey = 0;
        while (keySet.contains(newKey)) {
            newKey++;
        }
        return newKey;
    }

    @Override
    public void readZipFile(InputStream input) throws IOException {
        byte[] buffer = new byte[1024];
        ZipInputStream zis = new ZipInputStream(input);
        System.out.println(zis);
        ZipEntry nextEntry;
        Integer zipKey = createZipFile(new FileOutputStream("src/test/resources/zipfile/newZip.zip"));
        while ((nextEntry = zis.getNextEntry()) != null) {
            System.out.println(nextEntry.getName());
            if (nextEntry.getName().startsWith("test4_2021-12-02") || nextEntry.getName().startsWith("test4_2021-12-09")) {
                System.out.println("ENTRY was found!");
                FileOutputStream fileOutputStream = new FileOutputStream("src/test/resources/zipfile/selectedFile.pdf");
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                int len;
                while ((len = zis.read(buffer)) > 0) {
                    fileOutputStream.write(buffer, 0, len);
                    byteArrayOutputStream.write(buffer, 0, len);
                }
                ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
                log.debug(nextEntry.getName());
                addToZip(nextEntry.getName(), byteArrayInputStream, zipKey);

                fileOutputStream.close();
            }

        }
        close(zipKey);
    }
}
