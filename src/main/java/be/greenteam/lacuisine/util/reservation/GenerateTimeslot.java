package be.greenteam.lacuisine.util.reservation;

import be.greenteam.lacuisine.model.Openinghours;
import be.greenteam.lacuisine.model.Reservation;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Created by "Andy Van Camp" on 23/08/2021.
 */
@Getter
@Setter
@Slf4j
public class GenerateTimeslot {

    public Long getTotalOpeningTime(ChronoUnit unit, LocalTime openingTime, LocalTime closingTime) {
        Duration between = Duration.between(
                openingTime.truncatedTo(ChronoUnit.MINUTES),
                closingTime.truncatedTo(ChronoUnit.MINUTES));
        switch (unit) {
            case MINUTES:
                long minutes = between.toMinutes();
                return minutes;
            case HOURS:
                long hours = between.toHours();
                return hours;
            default:
                return null;
        }
    }

    /**
     * Generates timeslots between start time and end time by interval
     *
     * @param interval
     * @param openingTime
     * @param closingTime
     * @return List<Timeslot>
     */
    public List<Timeslot> getTimeslotList(int interval, LocalTime openingTime, LocalTime closingTime) {
        ArrayList<Timeslot> timeslotList = new ArrayList<>();
        LocalTime currentTime = closingTime.truncatedTo(ChronoUnit.MINUTES);
        while (currentTime.isAfter(openingTime)) {
            LocalTime startTimeslot = currentTime.minusMinutes(interval).truncatedTo(ChronoUnit.MINUTES);
            Timeslot timeslot = Timeslot.builder().startTime(startTimeslot).endTime(currentTime).build();
            if (timeslot.getStartTime().isAfter(openingTime.minusMinutes(1))) {
                timeslotList.add(timeslot);
                currentTime = timeslot.getStartTime();
            } else {
                currentTime = timeslot.getStartTime();
            }
        }
        return timeslotList;
    }

    /**
     * Generates timeslots from active opening hours
     *
     * @param openinghours
     * @return List<Timeslot>
     */
    public List<Timeslot> generateTimeslotMapForActive(Collection<Openinghours> openinghours) {
        ArrayList<Timeslot> timeslots = new ArrayList<>();
        openinghours.forEach(
                openinghour -> {
                    if (openinghour.isActive()) {
                        if (openinghour.getOpenTime() != null && openinghour.getCloseTime() != null) {
                            List<Timeslot> timeslotList = getTimeslotList(
                                    openinghour.getReservationTimeslot(),
                                    openinghour.getOpenTime(), openinghour.getCloseTime());
                            timeslots.addAll(timeslotList);
                        }
                    }
                }
        );
        Collections.reverse(timeslots);
        return timeslots;
    }

    /**
     * Generates list of available timeslots from opening hours and reservations
     *
     * @param openinghours
     * @param reservations
     * @param reservationDate
     * @return List<Timeslot>
     */
    public List<Timeslot> generateAvailableTimeslot(Collection<Openinghours> openinghours,
                                                    Collection<Reservation> reservations, LocalDate reservationDate) {
        List<Timeslot> timeslots = generateTimeslotMapForActive(openinghours);

        LocalTime timeNow = null;

        if (LocalDate.now().isEqual(reservationDate)) {
            timeNow = LocalTime.now();
        }

        if (reservations.size() == 0) {
            LocalTime finalTimeNow = timeNow;
            log.info("Time now: " + finalTimeNow);
            if (finalTimeNow != null) {
                timeslots.forEach(timeslot -> {
                    if (timeNowIsAfterStartTimeSlot(timeslot, finalTimeNow)) {
                        timeslot.setAvailable(false);
                    } else {
                        timeslot.setAvailable(true);
                    }
                });
            } else {
                timeslots.forEach(timeslot -> timeslot.setAvailable(true));
            }
        } else {
            LocalTime finalTimeNow = timeNow;
            timeslots.forEach(
                    timeslot -> {
                        for (Reservation reservation : reservations) {
                            if (checkOverlap(reservation, timeslot)) {
                                timeslot.setAvailable(false);
                                break;
                            } else {
                                if (finalTimeNow != null) {
                                    if (timeNowIsAfterStartTimeSlot(timeslot, finalTimeNow)) {
                                        timeslot.setAvailable(false);
                                    } else {
                                        timeslot.setAvailable(true);
                                    }
                                } else {
                                    timeslot.setAvailable(true);
                                }
                            }
                        }
                    }
            );
        }
        return timeslots;
    }

    private boolean timeNowIsAfterStartTimeSlot(Timeslot timeslot, LocalTime finalTimeNow) {
        if (finalTimeNow.isAfter(timeslot.getStartTime())) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Checks for overlap of reservation and timeslot
     *
     * @param reservation
     * @param timeslot
     * @return boolean
     */
    private boolean checkOverlap(Reservation reservation, Timeslot timeslot) {
        if ((timeslot.getStartTime().isAfter(reservation.getStartTime())
                && timeslot.getStartTime().isBefore(reservation.getEndTime())) ||
                (timeslot.getEndTime().isAfter(reservation.getStartTime())
                        && timeslot.getEndTime().isBefore(reservation.getEndTime()))) {
            return true;
        } else
            return false;
    }
}
