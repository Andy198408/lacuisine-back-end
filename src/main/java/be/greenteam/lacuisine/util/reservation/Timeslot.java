package be.greenteam.lacuisine.util.reservation;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalTime;

/**
 * Created by "Andy Van Camp" on 23/08/2021.
 */
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Timeslot {

    private LocalTime startTime;
    private LocalTime endTime;
    private boolean available;


}
