package be.greenteam.lacuisine.util.dataRetrieval;

import be.greenteam.lacuisine.model.FoodType;
import be.greenteam.lacuisine.model.OrderItem;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;

@Component
@Slf4j
public class RetrieveChartData {

    public void getTotalData(List<OrderItem> orderItems, HashMap<String, Integer> map) {
        orderItems.forEach(orderItem -> {
            String name = orderItem.getMenuItem().getName();
            if (map.containsKey(name)) {
                map.put(name, (map.get(name) + orderItem.getAmount()));
            } else {
                map.put(name, orderItem.getAmount());
            }
        });
    }

    public void getFoodData(List<OrderItem> orderItems, HashMap<String, Integer> map) {
        orderItems.forEach(orderItem -> {
            if (orderItem.getMenuItem().getFoodType() == FoodType.FOOD) {
                String name = orderItem.getMenuItem().getName();
                if (map.containsKey(name)) {
                    map.put(name, (map.get(name) + orderItem.getAmount()));
                } else {
                    map.put(name, orderItem.getAmount());
                }
            }
        });
    }

    public void getDrinkData(List<OrderItem> orderItems, HashMap<String, Integer> map) {
        orderItems.forEach(orderItem -> {
            if (orderItem.getMenuItem().getFoodType() == FoodType.DRINK) {
                String name = orderItem.getMenuItem().getName();
                if (map.containsKey(name)) {
                    map.put(name, (map.get(name) + orderItem.getAmount()));
                } else {
                    map.put(name, orderItem.getAmount());
                }
            }
        });
    }
}
