package be.greenteam.lacuisine.service;

import be.greenteam.lacuisine.model.FoodType;
import be.greenteam.lacuisine.model.MenuItem;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Transactional
@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class MenuItemServiceImplTest {
    @Autowired
    private MenuItemService menuItemService;
    @Autowired
    private MenuService menuService;
    private MenuItem menuItem1;
    private MenuItem menuItem2;
    private MenuItem menuItem3;
    private MenuItem menuItem4;



    @BeforeEach
    void setUp() {
        menuItem1 = new MenuItem(null,0,null,false,"cola2");
        menuItem2 = new MenuItem(null,0,null,false,"fanta2");
        menuItem3 = new MenuItem(null,0,null,false,"water2");
        menuItem4 = new MenuItem(null,0,null,false,"bier2");
    }

    @Test
    @Order(1)
    void save() {
        MenuItem menuItem = new MenuItem();
        menuItem.setMenu(menuService.findById(1L));
        MenuItem savedMenuItem = menuItemService.save(menuItem);
        MenuItem expectedMenuItem = savedMenuItem;
        MenuItem actualMenuItem = menuItemService.findById(savedMenuItem.getId());
        Assertions.assertEquals(expectedMenuItem,actualMenuItem);
        Assertions.assertDoesNotThrow(() ->menuItemService.save(menuItem));


    }

    @Test
    @Order(2)
    void saveAll() {
        List<MenuItem> itemList = new ArrayList<>();
        itemList.add(menuItem1);
        itemList.add(menuItem2);
        itemList.add(menuItem3);
        itemList.add(menuItem4);
        List<MenuItem> menuItems = menuItemService.saveAll(itemList);
        System.out.println(menuItemService.findAll().size());
        Assertions.assertEquals(menuItemService.findById(menuItems.get(0).getId()),menuItem1);
        Assertions.assertEquals(103,menuItemService.findAll().size());

    }

    @Test
   // @Transactional
    void findById() {
        //menu = null;
        MenuItem expectedMenuItem = new MenuItem(1L,menuService.findById(1L),1.8, FoodType.DRINK,true,"cola");
        MenuItem actualMenuItem = menuItemService.findById(1L);
        Assertions.assertEquals(expectedMenuItem,actualMenuItem); //fout opgelost -> Hash@Equals annotatie
        Assertions.assertEquals("cola",actualMenuItem.getName());
        Assertions.assertEquals(1,actualMenuItem.getMenu().getId());


    }

    @Test
    void findAll() {
        Assertions.assertEquals(99,menuItemService.findAll().size());
    }

    @Test
    void existsById() {
        Assertions.assertTrue(menuItemService.existsById(5L));
        Assertions.assertTrue(menuItemService.existsById(6L));
        Assertions.assertTrue(menuItemService.existsById(7L));
        Assertions.assertTrue(menuItemService.existsById(8L));
        Assertions.assertFalse(menuItemService.existsById(-1L));
        Assertions.assertFalse(menuItemService.existsById(0L));
        Assertions.assertFalse(menuItemService.existsById(100L));

    }

    @Test
   // @Transactional
   // @Rollback
    @Order(3)
    void deleteById() {
        menuItemService.deleteById(7L);
        Assertions.assertFalse(menuItemService.existsById(7L));
    }

    @Test
   // @Transactional
   // @Rollback
    @Order(4)
    void delete() {
        MenuItem menuItem = new MenuItem();
        menuItem.setMenu(null);
        MenuItem save = menuItemService.save(menuItem);
        menuItemService.delete(save);
        Assertions.assertFalse(menuItemService.existsById(save.getId()));

    }

    @Test
    void deleteAll() {
    }
}
