package be.greenteam.lacuisine.service;

import be.greenteam.lacuisine.model.RestaurantTable;
import be.greenteam.lacuisine.repository.RestaurantTableRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by "Andy Van Camp" on 25/08/2021.
 */



@SpringBootTest
@Transactional
class RestaurantTableServiceImplTest {

    private RestaurantTableServiceImpl restaurantTableService;

    @Autowired
    private RestaurantTableRepository restaurantTableRepository;

    @BeforeEach
    void setUp() {
        restaurantTableService = new RestaurantTableServiceImpl(restaurantTableRepository);

    }

    @Test
    void findAllByRestaurantIdAndAndCapacityIsGreaterThan() {
        List<RestaurantTable> tableList = restaurantTableService.findAllByRestaurantIdAndCapacity(1L, 2);
        System.out.println("############################################################################################");
        tableList.forEach(System.out::println);
        System.out.println("############################################################################################");

    }
}
