package be.greenteam.lacuisine.util.zip;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.*;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class ZipServiceImplTest {

    @Autowired
    ZipService zipService;

    FileInputStream fileInputStream;

    @BeforeEach
    void setUp() throws FileNotFoundException {
        fileInputStream = new FileInputStream("src/test/resources/zipfile/testFile.zip");
    }

    @Test
    void readZipFile() throws IOException {
        zipService.readZipFile(fileInputStream);
    }

    @Test
    void findAllEntriesZipFile() throws IOException {
        Set<String> allEntriesZipFile = zipService.findAllEntriesZipFile(fileInputStream);

        assertThat(allEntriesZipFile.size()).isEqualTo(3);
        assertThat(allEntriesZipFile.containsAll(
                List.of("test4_2021-12-02_created_19:45:17.796760200.pdf",
                        "test4_2021-12-09_created_19:45:17.800236200.pdf", "test4_2021-12-12_created_19:45:17.803722800.pdf"))).isTrue();
    }

    @Test
    void createNewZipOfEntries() throws IOException {

        FileInputStream findAllEntries = new FileInputStream("src/test/resources/zipfile/testFile.zip");

        Set<String> allEntriesInZipFile = zipService.findAllEntriesZipFile(findAllEntries);
        assertThat(allEntriesInZipFile.size()).isEqualTo(3);

        findAllEntries.close();

        Set<String> collectWantedEntries = allEntriesInZipFile.stream().filter(s -> s.contains("2_c")).collect(Collectors.toSet());
        System.out.println(collectWantedEntries);
        assertThat(collectWantedEntries.size()).isEqualTo(2);

        findAllEntries = new FileInputStream("src/test/resources/zipfile/testFile.zip");

        ByteArrayOutputStream zipByteArray = zipService.createNewZipOfEntries(collectWantedEntries, findAllEntries);
        File file = new File("src/test/resources/zipfile/testNewZipOfEntries.zip");

        zipService.writeByteArrayToFile(file, zipByteArray);

        assertThat(file.exists()).isTrue();
        assertThat(file.delete()).isTrue();

    }

    @Test
    void extractFilesFromZip() throws IOException {
        FileInputStream findAllEntries = new FileInputStream("src/test/resources/zipfile/testFile.zip");

        Set<String> allEntriesInZipFile = zipService.findAllEntriesZipFile(findAllEntries);
        assertThat(allEntriesInZipFile.size()).isEqualTo(3);

        findAllEntries.close();

        Set<String> collectWantedEntries = allEntriesInZipFile.stream().filter(s -> s.contains("2_c")).collect(Collectors.toSet());
        System.out.println(collectWantedEntries);
        assertThat(collectWantedEntries.size()).isEqualTo(2);

        findAllEntries = new FileInputStream("src/test/resources/zipfile/testFile.zip");
        File location = new File("src/test/resources/zipfile/");
        zipService.extractFilesFromZip(collectWantedEntries, findAllEntries, location);

    }
}