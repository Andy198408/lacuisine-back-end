package be.greenteam.lacuisine.util.reservation;

import be.greenteam.lacuisine.model.Openinghours;
import be.greenteam.lacuisine.model.Reservation;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by "Andy Van Camp" on 23/08/2021.
 */
class GenerateTimeslotTest {

    @Test
    void getTotalOpeningTime() {
        LocalTime now = LocalTime.now();
        LocalTime nowPlushour = LocalTime.now().plusHours(1);
        GenerateTimeslot timeGenerator = new GenerateTimeslot();

        assertEquals(60, timeGenerator.getTotalOpeningTime(ChronoUnit.MINUTES, now, nowPlushour));
        assertEquals(1, timeGenerator.getTotalOpeningTime(ChronoUnit.HOURS, now, nowPlushour));
    }

    @Test
    void getTimeslotList() {
        LocalTime now = LocalTime.now();
        LocalTime nowPlushour = LocalTime.now().plusHours(1).plusMinutes(10);
        GenerateTimeslot timeGenerator = new GenerateTimeslot();
        List<Timeslot> timeslotList = timeGenerator.getTimeslotList(15, now, nowPlushour);
        assertEquals(4, timeslotList.size());

        LocalTime now2 = LocalTime.now();
        LocalTime nowPlushour2 = LocalTime.now().plusHours(1).plusMinutes(14);
        GenerateTimeslot timeGenerator2 = new GenerateTimeslot();
        List<Timeslot> timeslotList2 = timeGenerator2.getTimeslotList(15, now2, nowPlushour2);
        assertEquals(4, timeslotList2.size());

        LocalTime now3 = LocalTime.now();
        LocalTime nowPlushour3 = LocalTime.now().plusHours(1).plusMinutes(16);
        GenerateTimeslot timeGenerator3 = new GenerateTimeslot();
        List<Timeslot> timeslotList3 = timeGenerator3.getTimeslotList(15, now3, nowPlushour3);
        assertEquals(5, timeslotList3.size());
    }

    @Test
    void generateTimeslotMapForActive() {
        LocalTime now = LocalTime.now();
        LocalTime nowPlus1 = now.plusHours(1);
        Openinghours openinghours = Openinghours.builder().reservationTimeslot(15).active(true)
                .openTime(now).closeTime(nowPlus1)
                .weekday(DayOfWeek.MONDAY)
                .build();
        Openinghours openinghours2 = Openinghours.builder().reservationTimeslot(15).active(true)
                .openTime(now).closeTime(nowPlus1)
                .weekday(DayOfWeek.TUESDAY)
                .build();
        Openinghours openinghours3 = Openinghours.builder().reservationTimeslot(15).active(true)
                .openTime(now).closeTime(nowPlus1)
                .weekday(DayOfWeek.WEDNESDAY)
                .build();
        Openinghours openinghours4 = Openinghours.builder().reservationTimeslot(15).active(true)
                .openTime(now).closeTime(nowPlus1)
                .weekday(DayOfWeek.THURSDAY)
                .build();
        Openinghours openinghours5 = Openinghours.builder().reservationTimeslot(15).active(true)
                .openTime(now).closeTime(nowPlus1)
                .weekday(DayOfWeek.FRIDAY)
                .build();

        List<Openinghours> openinghoursList = List.of(openinghours, openinghours2, openinghours3, openinghours4, openinghours5);

        GenerateTimeslot generateTimeslot = new GenerateTimeslot();
        List<Timeslot> timeslotMapForActive =
                generateTimeslot.generateTimeslotMapForActive(openinghoursList);

        assertEquals(20, timeslotMapForActive.size());
    }

    // todo implement new parameter in test
    @Test
    @Disabled
    void generateAvailableTimeslot() {
        LocalTime now = LocalTime.now();
        LocalTime nowPlus1 = now.plusHours(1);
        Openinghours openinghours = Openinghours.builder().reservationTimeslot(15).active(true)
                .openTime(now).closeTime(nowPlus1)
                .weekday(DayOfWeek.MONDAY)
                .build();

        List<Openinghours> openinghoursList = List.of(openinghours);

        Reservation reservation = Reservation.builder().startTime(now).endTime(now.plusMinutes(15)).build();
        Reservation reservation2 = Reservation.builder().startTime(now.plusMinutes(17)).endTime(now.plusMinutes(32)).build();
        Reservation reservation3 = Reservation.builder().startTime(now.plusMinutes(30)).endTime(now.plusMinutes(45)).build();
        Reservation reservation4 = Reservation.builder().startTime(now.plusMinutes(60)).endTime(now.plusMinutes(65)).build();
        List<Reservation> reservationList = List.of(reservation, reservation2, reservation3, reservation4);

        GenerateTimeslot generateTimeslot = new GenerateTimeslot();
        List<Timeslot> timeslots = generateTimeslot.generateAvailableTimeslot(openinghoursList, reservationList, null);

        System.out.println("reservation");
        System.out.println(reservation.getStartTime().truncatedTo(ChronoUnit.MINUTES) + "  "
                + reservation.getEndTime().truncatedTo(ChronoUnit.MINUTES));
        System.out.println("reservation2");
        System.out.println(reservation2.getStartTime().truncatedTo(ChronoUnit.MINUTES) + "  "
                + reservation2.getEndTime().truncatedTo(ChronoUnit.MINUTES));
        System.out.println("reservation3");
        System.out.println(reservation3.getStartTime().truncatedTo(ChronoUnit.MINUTES) + "  "
                + reservation3.getEndTime().truncatedTo(ChronoUnit.MINUTES));
        System.out.println("reservation4");
        System.out.println(reservation4.getStartTime().truncatedTo(ChronoUnit.MINUTES) + "  "
                + reservation4.getEndTime().truncatedTo(ChronoUnit.MINUTES));
        System.out.println(timeslots);

        assertEquals(4, timeslots.size());
        assertFalse(timeslots.get(0).isAvailable());
        assertFalse(timeslots.get(1).isAvailable());
        assertFalse(timeslots.get(2).isAvailable());
        assertTrue(timeslots.get(3).isAvailable());

    }

}
